#pragma once

#include <wx/wx.h>

#include <wx/splitter.h>
#include <wx/textctrl.h>
#include <wx/listctrl.h>
#include <wx/treectrl.h>

class Frame4 : public wxFrame
{
public:
    Frame4(const wxString &title, const wxPoint &pos, const wxSize &size);

    void OnTreeItemSelect(wxTreeEvent &event);

    void OnTreeItemSelect1(wxTreeEvent &event);

private:
    wxPanel *mainPanel;
    wxBoxSizer *mainSizer;

    wxPanel *upperPanel;
    wxBoxSizer *upperPanelSizer;

    wxPanel *lowerPanel;
    wxBoxSizer *lowerPanelSizer;

    wxTreeCtrl *treeCtrl;
    wxTreeCtrl *treeCtrl1;
    wxTextCtrl *descriptionCtrl;

    wxPanel *rightUpperPanel;
    wxBoxSizer *rightSizer;

    wxPanel *leftUpperPanel;
    wxBoxSizer *leftSizer;
};