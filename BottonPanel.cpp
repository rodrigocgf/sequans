#include "BottonPanel.h"

void BottonPanel::Scenario1()
{
    const auto margin = FromDIP(5);

    verticalSizer = new wxBoxSizer(wxVERTICAL);

    wxListCtrl *myList = new wxListCtrl(this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxLC_REPORT);

    // Add columns to the list
    myList->InsertColumn(0, "LOG                                                                   ");
    //myList->InsertColumn(1, "Column 2");

    // Add some sample data
    myList->InsertItem(0, "Item 1");
    //myList->SetItem(0, 1, "Value 1");
    myList->InsertItem(1, "Item 2");
    myList->InsertItem(2, "Item 3");
    myList->InsertItem(3, "Item 4");
    myList->InsertItem(4, "Item 5");
    myList->InsertItem(5, "Item 6");
    myList->InsertItem(6, "Item 7");
    myList->InsertItem(7, "Item 8");

    //myList->InsertItem(1, "Item 2");
    //myList->SetItem(1, 1, "Value 2");

    // mainSizer->Add(panelDownlink, 1, wxEXPAND | wxALL, margin);

    //verticalSizer->Add(myList, wxSizerFlags().Expand().Border(wxALL, 10));
    //verticalSizer->Add(myList, 0, wxEXPAND);
    verticalSizer->Add(myList, 0, wxEXPAND | wxALL, margin);

    SetBackgroundColour(wxColour(100, 100, 100)); // Set background color to dark gray
    SetSize(800, 300);
    SetSizerAndFit(verticalSizer);
    
}