#include "GraphPanel.h"

BEGIN_EVENT_TABLE(GraphPanel, wxPanel)
    EVT_PAINT(GraphPanel::OnPaint)
END_EVENT_TABLE()

void GraphPanel::OnPaint(wxPaintEvent &event)
{
    wxPaintDC dc(this);
    DrawGraph(dc);
}

void GraphPanel::DrawGraph(wxDC &dc)
{
    int width, height;
    GetSize(&width, &height);

    dc.SetBackground(wxBrush(wxColour(240, 240, 240)));
    dc.Clear();

    // Draw x-axis
    dc.SetPen(wxPen(wxColour(0, 0, 128), 2)); // Blue color for x-axis
    dc.DrawLine(0, height / 2, width, height / 2);
    dc.DrawLine(width - 10, height / 2 - 5, width, height / 2);
    dc.DrawLine(width - 10, height / 2 + 5, width, height / 2);

    dc.DrawText("t", width - 20, height / 2 + 5); // Label for x-axis

    // Draw y-axis
    dc.SetPen(wxPen(wxColour(0, 0, 128), 2)); // Blue color for y-axis
    dc.DrawLine(width / 2, 0, width / 2, height);
    dc.DrawLine(width / 2 - 5, 10, width / 2, 0);
    dc.DrawLine(width / 2 + 5, 10, width / 2, 0);

    dc.DrawText("s(t)", width / 2 + 5, 5); // Label for y-axis

    // Scale and draw the graph
    dc.SetPen(wxPen(wxColour(0, 0, 0), 1)); // Black color for the graph
    

    // Draw grid
    for (int i = -width ; i < width ; i += 20)
    {
        dc.DrawLine(i, -height , i, height );
    }
    for (int i = -height ; i < height ; i += 20)
    {
        dc.DrawLine(-width, i, width, i);
    }

    dc.SetPen(wxPen(wxColour(128, 0, 0), 2)); // Red color for the graph

    double scale = height / 2.0;

    for (double t = 0; t < width; t += 0.1)
    {
        //double value = TriangularWave(static_cast<double>(t - width / 2) / 10.0); // OK
        //double value = SinusoidWave(static_cast<double>(t - width / 2) / 10.0); // OK
        double value = SquareWave(static_cast<double>(t - width / 2) / 10.0); // OK

        int y = height / 2 - static_cast<int>(value * scale / height);
        dc.DrawPoint(t, y);
    }
}

double GraphPanel::SinusoidWave(double t)
{
    double Amplitude = 300.0;
    double w0 = 2.0;
    double pi = 3.14159265358979323846;
    return Amplitude / 4 + Amplitude * std::sqrt(2) / pi * std::cos(w0 * t);
}

double GraphPanel::TriangularWave(double t)
{
    double Amplitude = 200.0;
    double w0 = 0.5;
    double result = 0.0;
    for (int n = 1; n <= 200; n += 2)
    {
        result += (4.0 / (n * M_PI)) * sin(w0 * (2.0 * n - 1) * t) / (2.0 * n - 1);
    }

    return Amplitude * result;
}

double GraphPanel::SquareWave(double t)
{
    double Amplitude = 200.0;
    double w0 = 0.4;
    double d = 0.5;
    double result = 0.0;
    for (int n = 1; n <= 50; n += 2)
    {
        result += (2.0 / (n * M_PI)) * sin(M_PI * n * d) * cos(w0 * n * t);
    }

    return Amplitude * result;
}