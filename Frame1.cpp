#include "Frame1.h"

Frame1::Frame1(const wxString &title, const wxPoint &pos, const wxSize &size)
    : wxFrame(NULL, wxID_ANY, title, pos, size)
{

    // Create a splitter window for the upper panel
    wxSplitterWindow *splitter = new wxSplitterWindow(this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxSP_LIVE_UPDATE);
    splitter->SetSashGravity(0.5); // Split the upper panel evenly

    // Create the left panel for the list
    leftPanel = new wxPanel(splitter, wxID_ANY);
    leftSizer = new wxBoxSizer(wxVERTICAL);

    treeCtrl = new wxTreeCtrl(leftPanel, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTR_DEFAULT_STYLE);

    wxTreeItemId rootItem4G = treeCtrl->AddRoot("4G");
    wxTreeItemId subItem4G_1 = treeCtrl->AppendItem(rootItem4G, "1)");
    treeCtrl->AppendItem(subItem4G_1, "1.1)");
    treeCtrl->AppendItem(subItem4G_1, "1.2)");

    wxTreeItemId subItem4G_2 = treeCtrl->AppendItem(rootItem4G, "2)");
    treeCtrl->AppendItem(subItem4G_2, "2.1)");
    treeCtrl->AppendItem(subItem4G_2, "2.2)");

    treeCtrl1 = new wxTreeCtrl(leftPanel, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTR_DEFAULT_STYLE);
    wxTreeItemId rootItem5G = treeCtrl1->AddRoot("5G");
    wxTreeItemId subItem5G_1 = treeCtrl1->AppendItem(rootItem5G, "1)");
    treeCtrl1->AppendItem(subItem5G_1, "1.1)");
    treeCtrl1->AppendItem(subItem5G_1, "1.2)");

    wxTreeItemId subItem5G_2 = treeCtrl1->AppendItem(rootItem5G, "2)");
    treeCtrl1->AppendItem(subItem5G_2, "2.1)");
    treeCtrl1->AppendItem(subItem5G_2, "2.2)");

    treeCtrl->Bind(wxEVT_TREE_SEL_CHANGED, &Frame1::OnTreeItemSelect, this);
    treeCtrl1->Bind(wxEVT_TREE_SEL_CHANGED, &Frame1::OnTreeItemSelect1, this);

    // Add items and subitems to the listCtrl here
    leftSizer->Add(treeCtrl, 1, wxEXPAND);
    leftSizer->Add(treeCtrl1, 2, wxEXPAND);
    leftPanel->SetSizer(leftSizer);

    // Create the right panel for the description
    rightPanel = new wxPanel(splitter, wxID_ANY);
    rightSizer = new wxBoxSizer(wxVERTICAL);

    descriptionCtrl = new wxTextCtrl(rightPanel, wxID_ANY, "", wxDefaultPosition, wxDefaultSize, wxTE_MULTILINE | wxTE_READONLY | wxVSCROLL);
    rightSizer->Add(descriptionCtrl, 1, wxEXPAND);

    rightPanel->SetSizer(rightSizer);

    // Set the left and right panels in the splitter window
    splitter->SplitVertically(leftPanel, rightPanel);

    // Create a status bar
    CreateStatusBar();

    // Set up menu and other event handling here
}

void Frame1::OnTreeItemSelect(wxTreeEvent &event)
{
    wxTreeItemId itemId = event.GetItem();
    if (itemId.IsOk())
    {
        wxString text = treeCtrl->GetItemText(itemId);
        descriptionCtrl->Clear();
        for (int i = 1; i <= 100; i++)
        {
            descriptionCtrl->AppendText("[" + text + "] : " + wxString::Format("%d", i) + "\n");
        }
    }
}

void Frame1::OnTreeItemSelect1(wxTreeEvent &event)
{
    // Handle item selection here
    wxTreeItemId itemId = event.GetItem();
    if (itemId.IsOk())
    {
        wxString text = treeCtrl1->GetItemText(itemId);
        wxMessageBox("Selected Item: " + text, "Item Selected");
    }
}