#include "sqlite3.h"

bool SQLite::open()
{
    int rc = sqlite3_open(dbName.c_str(), &db);
    return (rc == SQLITE_OK);
}

void SQLite::close()
{
    if (db)
    {
        sqlite3_close(db);
        db = nullptr;
    }
}

bool SQLite::executeQuery(const std::string &query)
{
    if (!db)
    {
        return false;
    }

    char *errorMsg = nullptr;
    int rc = sqlite3_exec(db, query.c_str(), nullptr, nullptr, &errorMsg);

    if (rc != SQLITE_OK)
    {
        std::cerr << "SQL error: " << errorMsg << std::endl;
        sqlite3_free(errorMsg);
        return false;
    }

    return true;
}

bool SQLite::executeSelect(const std::string &query, std::string &result)
{
    if (!db)
    {
        return false;
    }

    sqlite3_stmt *stmt = nullptr;
    int rc = sqlite3_prepare_v2(db, query.c_str(), -1, &stmt, nullptr);

    if (rc == SQLITE_OK)
    {
        result = ""; // Clear the result string
        while (sqlite3_step(stmt) == SQLITE_ROW)
        {
            const char *data = reinterpret_cast<const char *>(sqlite3_column_text(stmt, 0));
            result += data;
            result += "\n";
        }
        sqlite3_finalize(stmt);
        return true;
    }
    else
    {
        std::cerr << "SQL error: " << sqlite3_errmsg(db) << std::endl;
        return false;
    }
}