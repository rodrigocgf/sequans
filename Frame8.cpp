#include "Frame8.h"

Frame8::Frame8(const wxString &title, const wxPoint &pos, const wxSize &size)
    : wxFrame(NULL, wxID_ANY, title, pos, size)
{
    Scenario2();
}

void Frame8::Scenario1()
{
    UpperLeftPanel *upperPanel = new UpperLeftPanel(this);
    MiddlePanel *middlePanel = new MiddlePanel(this);
    WaveformPanel *waveformPanel = new WaveformPanel(this, middlePanel);
    // GraphPanel *graphPanel = new GraphPanel(this);

    // std::function<double(double)> f_timeFuntion = &Frame8::TriangularWave;
    // std::function<double(double)> f_timeFuntion = &Frame8::SinusoidWave;
    std::function<double(double)> f_timeFuntion = &Frame8::SquareWave;
    GraphPanel<Time, 400, 300> *graphPanel = new GraphPanel<Time, 400, 300>(this, f_timeFuntion);

    // Set up sizer for the frame
    wxBoxSizer *verticalSizer = new wxBoxSizer(wxVERTICAL);

    wxBoxSizer *horizontalUpperSizer = new wxBoxSizer(wxHORIZONTAL);
    horizontalUpperSizer->Add(upperPanel, wxSizerFlags().Expand().Border(wxALL, 10));
    horizontalUpperSizer->Add(waveformPanel, wxSizerFlags().Expand().Border(wxALL, 10));

    verticalSizer->Add(horizontalUpperSizer, wxSizerFlags().Expand().Border(wxALL, 10));

    verticalSizer->Add(graphPanel, wxSizerFlags().Expand().Border(wxALL, 10));
    verticalSizer->Add(middlePanel, wxSizerFlags().Expand().Border(wxALL, 10));

    SetSizerAndFit(verticalSizer);

    // wxMessageBox("Frame 8", "Info", wxOK | wxICON_INFORMATION); // OK
}

void Frame8::Scenario2()
{
    UpperLeftPanel *upperPanel = new UpperLeftPanel(this);
    MiddlePanel *middlePanel = new MiddlePanel(this);
    WaveformPanel *waveformPanel = new WaveformPanel(this, middlePanel);

    std::function<double(double)> f_TriangularWave = &Frame8::TriangularWave;
     std::function<double(double)> f_SinusoidWave = &Frame8::SinusoidWave;
    std::function<double(double)> f_SquareWave = &Frame8::SquareWave;
    GraphPanel<Time, 400, 300> *graphPanel = new GraphPanel<Time, 400, 300>(this, f_SquareWave);
    //GraphPanel<Frequency, 400, 300> *graphPanel1 = new GraphPanel<Frequency, 400, 300>(this, f_SinusoidWave);
    //GraphPanel<Time, 400, 300> *graphPanel1 = new GraphPanel<Time, 400, 300>(this, f_SinusoidWave);
    GraphPanel<Time, 400, 300> *graphPanel1 = new GraphPanel<Time, 400, 300>(this, f_TriangularWave);

    // Set up sizer for the frame
    wxBoxSizer *verticalSizer = new wxBoxSizer(wxVERTICAL);

    wxBoxSizer *horizontalUpperSizer = new wxBoxSizer(wxHORIZONTAL);
    horizontalUpperSizer->Add(upperPanel, wxSizerFlags().Expand().Border(wxALL, 10));
    horizontalUpperSizer->Add(waveformPanel, wxSizerFlags().Expand().Border(wxALL, 10));

    verticalSizer->Add(horizontalUpperSizer, wxSizerFlags().Expand().Border(wxALL, 10));

    wxBoxSizer *horizontalMiddleSizer = new wxBoxSizer(wxHORIZONTAL);
    horizontalMiddleSizer->Add(graphPanel, wxSizerFlags().Expand().Border(wxALL, 10));
    horizontalMiddleSizer->Add(graphPanel1, wxSizerFlags().Expand().Border(wxALL, 10));

    verticalSizer->Add(horizontalMiddleSizer, wxSizerFlags().Expand().Border(wxALL, 10));
    verticalSizer->Add(middlePanel, wxSizerFlags().Expand().Border(wxALL, 10));

    SetSizerAndFit(verticalSizer);

}

double Frame8::SinusoidWave(double t)
{
    double Amplitude = 300.0;
    double w0 = 0.4;

    return Amplitude * cos(w0 * t);
    //return Amplitude * cos(w0 * t + M_PI/4);
}

double Frame8::SinusoidWaveAlt(double t)
{
    double Amplitude = 300.0;
    double w0 = 2.0;
    double pi = 3.14159265358979323846;
    return Amplitude / 4 + Amplitude * std::sqrt(2) / pi * std::cos(w0 * t);
}

double Frame8::TriangularWave(double t)
{
    double Amplitude = 200.0;
    double w0 = 0.5;
    double result = 0.0;
    for (int n = 1; n <= 200; n += 2)
    {
        result += (4.0 / (n * M_PI)) * sin(w0 * (2.0 * n - 1) * t) / (2.0 * n - 1);
    }

    return Amplitude * result;
}

double Frame8::SquareWave(double t)
{
    double Amplitude = 200.0;
    double w0 = 0.4;
    double d = 0.5;
    double result = 0.0;
    for (int n = 1; n <= 50; n += 2)
    {
        result += (2.0 / (n * M_PI)) * sin(M_PI * n * d) * cos(w0 * n * t);
    }

    return Amplitude * result;
}