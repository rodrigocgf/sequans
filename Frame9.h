#pragma once

#include <functional>

#include <wx/gbsizer.h>
#include <wx/wx.h>

#include "UpperLeftPanel.h"
#include "WaveformPanel.h"
#include "MiddlePanel.h"
// #include "GraphPanel.h"
#include "GraphPanel.hpp"
#include "BottonPanel.h"

class Frame9 : public wxFrame
{
public:
    Frame9(const wxString &title, const wxPoint &pos, const wxSize &size);

private:
    void Scenario1();
    void Scenario2();

    static double TriangularWave(double t);
    static double SquareWave(double t);
    static double SinusoidWave(double t);
    static double SinusoidWaveAlt(double t);
};