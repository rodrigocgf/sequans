#pragma once

#include <wx/wx.h>
#include <wx/gbsizer.h>
#include <wx/panel.h>
#include <wx/sizer.h>
#include <wx/tglbtn.h>

class DownlinkPanel : public wxPanel
{
public:
    DownlinkPanel(wxWindow *parent, wxWindowID id = wxID_ANY, const wxPoint &pos = wxDefaultPosition, const wxSize &size = wxDefaultSize)
        : wxPanel(parent, id, pos, size)
    {
        Scenario1();
    }

    void ShowContentDownlink(bool expanded = true);

private:
    void Scenario1();
    void OnArrowClickDownlink(wxMouseEvent &event);
    void CreateArrowDownlink(wxPanel *panel);
    void CreateLabelsDownlink(wxPanel *panel);
    void AddElementsToSizerDownlink(const int margin);

    wxBoxSizer *mainSizer;

    bool expandedDownlink;

    wxPanel *panelDownlink;

    wxStaticText *arrowLabelDownlink;
    wxGridBagSizer *arrowLabelDownlinkSizer;
    wxGridBagSizer *contentSizerDownlink;

    using ST_DOWNLINK = struct
    {
        wxStaticText *label;
        wxStaticText *frequenceRangeLabel;
        wxStaticText *channelBandwidthLabel;
        wxStaticText *cellIdentityLabel;
        wxStaticText *subframesLabel;
        wxStaticText *initialSubframeLabel;
        wxStaticText *windowingSourceLabel;
        wxStaticText *windowingLabel;
    };

    ST_DOWNLINK m_Downlink;

};