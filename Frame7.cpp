#include "Frame7.h"

Frame7::Frame7(const wxString &title, const wxPoint &pos, const wxSize &size)
    : wxFrame(nullptr, wxID_ANY, title, pos, size)
{
    //Scenario1(); // OK
    Scenario2();
}
void Frame7::Scenario1()
{
    const auto margin = FromDIP(10);

    auto mainSizer = new wxBoxSizer(wxVERTICAL);
    wxPanel *panel = new wxPanel(this, wxID_ANY);
    SetBackgroundColour(panel->GetBackgroundColour());

    expandedUplinkFRC = false;
    CreateLabelsUplinkFRC(panel);
    CreateComboBoxesUplinkFRC(panel);

    arrowLabelUplinkFRCSizer = new wxGridBagSizer(margin, margin);

    AddElementsToSizerUplinkFRC(margin);

    ShowContentUplinkFRC(expandedUplinkFRC);

    panel->SetSizer(arrowLabelUplinkFRCSizer);

    mainSizer->Add(panel, 1, wxEXPAND | wxALL, margin);
    SetSizerAndFit(mainSizer);

    SetSize(1200, 800);
}

void Frame7::Scenario2()
{
    const auto margin = FromDIP(10);

    mainSizer = new wxBoxSizer(wxVERTICAL);
    panelUplinkFRC = new wxPanel(this, wxID_ANY);
    panelDownlink = new wxPanel(this, wxID_ANY);
    SetBackgroundColour(panelUplinkFRC->GetBackgroundColour());
    SetBackgroundColour(panelDownlink->GetBackgroundColour());

    expandedUplinkFRC = false;
    expandedDownlink = false;

    CreateArrowUplinkFRC(panelUplinkFRC);
    CreateLabelsUplinkFRC(panelUplinkFRC);
    CreateComboBoxesUplinkFRC(panelUplinkFRC);

    CreateArrowDownlink(panelDownlink);
    CreateLabelsDownlink(panelDownlink);

    arrowLabelUplinkFRCSizer = new wxGridBagSizer(margin, margin);
    arrowLabelDownlinkSizer = new wxGridBagSizer(margin, margin);

    AddElementsToSizerUplinkFRC(margin);
    AddElementsToSizerDownlink(margin);

    ShowContentUplinkFRC(expandedUplinkFRC);
    ShowContentDownlink(expandedDownlink);

    panelUplinkFRC->SetSizer(arrowLabelUplinkFRCSizer);
    panelDownlink->SetSizer(arrowLabelDownlinkSizer);

    mainSizer->Add(panelDownlink, 1, wxEXPAND | wxALL, margin);
    mainSizer->Add(panelUplinkFRC, 10, wxEXPAND | wxALL, margin);
    // mainSizer->Add(panelDownlink, 1, wxALL, margin);
    // mainSizer->Add(panelUplinkFRC, 2, wxALL, margin);
    SetSizerAndFit(mainSizer);

    SetSize(1200, 800);
}

void Frame7::OnArrowClickUplinkFRC(wxMouseEvent &event)
{
    wxString label = "Uplink FRC";

    expandedUplinkFRC = !expandedUplinkFRC;
    if (expandedUplinkFRC)
        arrowLabelUplinkFRC->SetLabel(label + "   \u25BC");
    else
    {
        arrowLabelUplinkFRC->SetLabel(label + "   \u25B6");
    }

    ShowContentUplinkFRC(expandedUplinkFRC);
    Layout();

    //contentSizerUplinkFRC->Layout();
    //Fit();
}

void Frame7::OnArrowClickDownlink(wxMouseEvent &event)
{
    wxString label = "Downlink";

    expandedDownlink = !expandedDownlink;
    if (expandedDownlink)
    {
        arrowLabelDownlink->SetLabel(label + "   \u25BC");
    }
    else
    {
        arrowLabelDownlink->SetLabel(label + "   \u25B6");
        panelDownlink->SetSize(1, 1);
    }

    ShowContentDownlink(expandedDownlink);
    Layout();

    // contentSizerDownlink->Layout();
    // mainSizer->Layout();
    //Fit();

}

void Frame7::ShowContentUplinkFRC(bool expanded)
{
    m_UplinkFrc.frequenceRangeLabel->Show(expanded);
    m_UplinkFrc.mcsLabel->Show(expanded);
    m_UplinkFrc.fixedReferenceChannelLabel->Show(expanded);
    m_UplinkFrc.channelBandwidthLabel->Show(expanded);
    m_UplinkFrc.duplexModeLabel->Show(expanded);
    m_UplinkFrc.puschLocationLabel->Show(expanded);
    m_UplinkFrc.rbOffsetLabel->Show(expanded);
    m_UplinkFrc.subframesLabel->Show(expanded);
    m_UplinkFrc.cellIdentityLabel->Show(expanded);
    m_UplinkFrc.rntiLabel->Show(expanded);

    m_UplinkFrc.windowingSourceLabel->Show(expanded);
    m_UplinkFrc.windowingLabel->Show(expanded);
    m_UplinkFrc.sampleRateSourceLabel->Show(expanded);

    m_UplinkFrc.m_cbFreqRange->Show(expanded);
    m_UplinkFrc.m_cbMcs->Show(expanded);
    m_UplinkFrc.m_cbFixedRefChannel->Show(expanded);
    m_UplinkFrc.m_cbDuplexMode->Show(expanded);
}

void Frame7::ShowContentDownlink(bool expanded)
{
    m_Downlink.label->Show(expanded);
    m_Downlink.frequenceRangeLabel->Show(expanded);
    m_Downlink.channelBandwidthLabel->Show(expanded);
    m_Downlink.cellIdentityLabel->Show(expanded);
    m_Downlink.subframesLabel->Show(expanded);
    m_Downlink.initialSubframeLabel->Show(expanded);
    m_Downlink.windowingSourceLabel->Show(expanded);
    m_Downlink.windowingLabel->Show(expanded);
}



void Frame7::CreateLabelsUplinkFRC(wxPanel *panel)
{
    m_UplinkFrc.frequenceRangeLabel = new wxStaticText(panel, wxID_ANY, "Frequency range: ");
    m_UplinkFrc.mcsLabel = new wxStaticText(panel, wxID_ANY, "MCS: ");
    m_UplinkFrc.fixedReferenceChannelLabel = new wxStaticText(panel, wxID_ANY, "Fixed reference channel: ");
    m_UplinkFrc.channelBandwidthLabel = new wxStaticText(panel, wxID_ANY, "Channel bandwidth (MHz): ");
    m_UplinkFrc.duplexModeLabel = new wxStaticText(panel, wxID_ANY, "Duplex mode: ");
    m_UplinkFrc.puschLocationLabel = new wxStaticText(panel, wxID_ANY, "PUSH location: ");
    m_UplinkFrc.rbOffsetLabel = new wxStaticText(panel, wxID_ANY, "RB offset: ");
    m_UplinkFrc.subframesLabel = new wxStaticText(panel, wxID_ANY, "Subframes: ");
    m_UplinkFrc.cellIdentityLabel = new wxStaticText(panel, wxID_ANY, "Cell identity: ");
    m_UplinkFrc.rntiLabel = new wxStaticText(panel, wxID_ANY, "RNTI: ");

    m_UplinkFrc.windowingSourceLabel = new wxStaticText(panel, wxID_ANY, "Windowing source: ");
    m_UplinkFrc.windowingLabel = new wxStaticText(panel, wxID_ANY, "Windowing (%): ");
    m_UplinkFrc.sampleRateSourceLabel = new wxStaticText(panel, wxID_ANY, "Sample rate source: ");
}

void Frame7::CreateArrowUplinkFRC(wxPanel *panel)
{
    wxString label = "Uplink FRC";
    arrowLabelUplinkFRC = new wxStaticText(panel, wxID_ANY, label + "   \u25B6", wxDefaultPosition, wxDefaultSize, wxALIGN_RIGHT);

    wxFont font = arrowLabelUplinkFRC->GetFont();
    font.MakeBold();
    arrowLabelUplinkFRC->SetFont(font);
    arrowLabelUplinkFRC->Bind(wxEVT_LEFT_DOWN, &Frame7::OnArrowClickUplinkFRC, this);
}

void Frame7::CreateArrowDownlink(wxPanel *panel)
{
    wxString label = "Downlink";
    arrowLabelDownlink = new wxStaticText(panel, wxID_ANY, label + "   \u25B6", wxDefaultPosition, wxDefaultSize, wxALIGN_RIGHT);

    wxFont font = arrowLabelDownlink->GetFont();
    font.MakeBold();
    arrowLabelDownlink->SetFont(font);
    arrowLabelDownlink->Bind(wxEVT_LEFT_DOWN, &Frame7::OnArrowClickDownlink, this);
}

void Frame7::CreateLabelsDownlink(wxPanel *panel)
{

    m_Downlink.label = new wxStaticText(panel, wxID_ANY, "Label: ");
    m_Downlink.frequenceRangeLabel = new wxStaticText(panel, wxID_ANY, "Frequency range: ");
    m_Downlink.channelBandwidthLabel = new wxStaticText(panel, wxID_ANY, "Channel bandwidth (MHz): ");
    m_Downlink.cellIdentityLabel = new wxStaticText(panel, wxID_ANY, "Cell identity: ");
    m_Downlink.subframesLabel = new wxStaticText(panel, wxID_ANY, "Subframes: ");
    m_Downlink.initialSubframeLabel = new wxStaticText(panel, wxID_ANY, "Initial subframe: ");

    m_Downlink.windowingSourceLabel = new wxStaticText(panel, wxID_ANY, "Windowing source: ");
    m_Downlink.windowingLabel = new wxStaticText(panel, wxID_ANY, "Windowing (%): ");
}


void Frame7::CreateComboBoxesUplinkFRC(wxPanel *panel)
{
    wxString choices[] = {
        "FR1(410 MHz - 7.125 GHz)",
        "FR2(24.25 GHz - 71 GHz)"};
    m_UplinkFrc.m_cbFreqRange = new wxComboBox(panel, wxID_ANY, choices[0], wxDefaultPosition,
                                              wxDefaultSize, WXSIZEOF(choices), choices, wxCB_DROPDOWN);
    m_UplinkFrc.m_cbFreqRange->SetSelection(0);

    wxString choicesMcs[] = {
        "QPSK, R=1/3",
        "16QAM, R=2/3",
        "QPSK, R=193/1024",
        "16QAM, R=658/1024",
        "64QAM, R=567/1024"};

    m_UplinkFrc.m_cbMcs = new wxComboBox(panel, wxID_ANY, choicesMcs[0], wxDefaultPosition,
                                        wxDefaultSize, WXSIZEOF(choicesMcs), choicesMcs, wxCB_DROPDOWN);
    m_UplinkFrc.m_cbMcs->SetSelection(0);

    wxString choiceFixedRefChannel[] = {
        "G-FR1-A5-1 (15 kHz SCS, 25 RBs)",
        "G-FR1-A5-2 (15 kHz SCS, 52 RBs)",
        "G-FR1-A5-3 (15 kHz SCS, 106 RBs)",
        "G-FR1-A5-4 (30 kHz SCS, 24 RBs)",
        "G-FR1-A5-5 (30 kHz SCS, 51 RBs)",
        "G-FR1-A5-6 (30 kHz SCS, 106 RBs)",
        "G-FR1-A5-7 (30 kHz SCS, 273 RBs)",
        "G-FR1-A5-8 (15 kHz SCS, 25 RBs)",
        "G-FR1-A5-9 (15 kHz SCS, 52 RBs)",
        "G-FR1-A5-10 (15 kHz SCS, 106 RBs)",
        "G-FR1-A5-11 (30 kHz SCS, 24 RBs)",
        "G-FR1-A5-12 (30 kHz SCS, 51 RBs)",
        "G-FR1-A5-13 (30 kHz SCS, 106 RBs)",
        "G-FR1-A5-14 (30 kHz SCS, 273 RBs)"};
    m_UplinkFrc.m_cbFixedRefChannel = new wxComboBox(panel, wxID_ANY, choiceFixedRefChannel[0], wxDefaultPosition,
                                                    wxDefaultSize, WXSIZEOF(choiceFixedRefChannel), choiceFixedRefChannel, wxCB_DROPDOWN);
    m_UplinkFrc.m_cbFixedRefChannel->SetSelection(0);

    wxString choicesDuplexMode[] = {
        "TDD",
        "FDD"};
    m_UplinkFrc.m_cbDuplexMode = new wxComboBox(panel, wxID_ANY, choicesDuplexMode[0], wxDefaultPosition,
                                               wxDefaultSize, WXSIZEOF(choicesDuplexMode), choicesDuplexMode, wxCB_DROPDOWN);
    m_UplinkFrc.m_cbDuplexMode->SetSelection(0);
}

void Frame7::AddElementsToSizerUplinkFRC(const int margin)
{
    contentSizerUplinkFRC = new wxGridBagSizer(margin, margin);
    contentSizerUplinkFRC->Add(m_UplinkFrc.frequenceRangeLabel, {1, 0}, {1, 1}, wxALIGN_RIGHT);
    contentSizerUplinkFRC->Add(m_UplinkFrc.m_cbFreqRange, {1, 1}, {1, 1}, wxALIGN_LEFT);

    contentSizerUplinkFRC->Add(m_UplinkFrc.mcsLabel, {2, 0}, {1, 1}, wxALIGN_RIGHT);
    contentSizerUplinkFRC->Add(m_UplinkFrc.m_cbMcs, {2, 1}, {1, 1}, wxALIGN_LEFT);

    contentSizerUplinkFRC->Add(m_UplinkFrc.fixedReferenceChannelLabel, {3, 0}, {1, 1}, wxALIGN_RIGHT);
    contentSizerUplinkFRC->Add(m_UplinkFrc.m_cbFixedRefChannel, {3, 1}, {1, 1}, wxALIGN_LEFT);

    contentSizerUplinkFRC->Add(m_UplinkFrc.channelBandwidthLabel, {4, 0}, {1, 1}, wxALIGN_RIGHT);

    contentSizerUplinkFRC->Add(m_UplinkFrc.duplexModeLabel, {5, 0}, {1, 1}, wxALIGN_RIGHT);
    contentSizerUplinkFRC->Add(m_UplinkFrc.m_cbDuplexMode, {5, 1}, {1, 1}, wxALIGN_LEFT);

    contentSizerUplinkFRC->Add(m_UplinkFrc.puschLocationLabel, {6, 0}, {1, 1}, wxALIGN_RIGHT);
    contentSizerUplinkFRC->Add(m_UplinkFrc.rbOffsetLabel, {7, 0}, {1, 1}, wxALIGN_RIGHT);
    contentSizerUplinkFRC->Add(m_UplinkFrc.subframesLabel, {8, 0}, {1, 1}, wxALIGN_RIGHT);
    contentSizerUplinkFRC->Add(m_UplinkFrc.cellIdentityLabel, {9, 0}, {1, 1}, wxALIGN_RIGHT);
    contentSizerUplinkFRC->Add(m_UplinkFrc.rntiLabel, {10, 0}, {1, 1}, wxALIGN_RIGHT);

    contentSizerUplinkFRC->Add(m_UplinkFrc.windowingSourceLabel, {12, 0}, {1, 1}, wxALIGN_RIGHT);
    contentSizerUplinkFRC->Add(m_UplinkFrc.windowingLabel, {13, 0}, {1, 1}, wxALIGN_RIGHT);
    contentSizerUplinkFRC->Add(m_UplinkFrc.sampleRateSourceLabel, {14, 0}, {1, 1}, wxALIGN_RIGHT);

    arrowLabelUplinkFRCSizer->Add(arrowLabelUplinkFRC, 0, wxUP, margin);
    arrowLabelUplinkFRCSizer->Add(contentSizerUplinkFRC, 1, wxDOWN, margin);
}

void Frame7::AddElementsToSizerDownlink(const int margin)
{
    contentSizerDownlink = new wxGridBagSizer(margin, margin);
    contentSizerDownlink->Add(m_Downlink.label, {1, 0}, {1, 1}, wxALIGN_RIGHT);
    contentSizerDownlink->Add(m_Downlink.frequenceRangeLabel, {2, 0}, {1, 1}, wxALIGN_RIGHT);
    contentSizerDownlink->Add(m_Downlink.channelBandwidthLabel, {3, 0}, {1, 1}, wxALIGN_RIGHT);
    contentSizerDownlink->Add(m_Downlink.cellIdentityLabel, {4, 0}, {1, 1}, wxALIGN_RIGHT);
    contentSizerDownlink->Add(m_Downlink.subframesLabel, {5, 0}, {1, 1}, wxALIGN_RIGHT);
    contentSizerDownlink->Add(m_Downlink.initialSubframeLabel, {6, 0}, {1, 1}, wxALIGN_RIGHT);

    contentSizerDownlink->Add(m_Downlink.windowingSourceLabel, {7, 0}, {1, 1}, wxALIGN_RIGHT);
    contentSizerDownlink->Add(m_Downlink.windowingLabel, {8, 0}, {1, 1}, wxALIGN_RIGHT);

    arrowLabelDownlinkSizer->Add(arrowLabelDownlink, 0, wxUP, margin);
    arrowLabelDownlinkSizer->Add(contentSizerDownlink, 1, wxDOWN, margin);
}