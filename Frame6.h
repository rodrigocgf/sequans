#pragma once

#include <wx/wx.h>

#include <wx/gbsizer.h>
#include <vector>

#include <wx/file.h>
#include <wx/filename.h>
#include <wx/filedlg.h>
#include <wx/splitter.h>
#include <wx/textctrl.h>
#include <wx/listctrl.h>
#include <wx/treectrl.h>
#include <wx/combobox.h>
#include "bufferedbitmap.h"

class Frame6 : public wxFrame
{
public:
    enum
    {
        ComboPage_FreqRange = wxID_HIGHEST,
        ComboPage_Mcs,
        ComboPage_FixedRefChannel,
        ComboPage_DuplexMode
    };

    Frame6(const wxString &title, const wxPoint &pos, const wxSize &size);

    // void OnTreeItemSelect(wxTreeEvent &event);
    // void OnTreeItemSelect1(wxTreeEvent &event);

private:
    void Scenario1();
    void CreateComboBoxesUplinkFRC(wxPanel *panel);
    void CreateLabelsUplinkFRC(wxPanel *panel);
    void AddElementsToSizerUplinkFRC(wxGridBagSizer *sizer, const int margin);
    void AddElementsToSizerUplinkFRCAlt(wxGridBagSizer *sizer, const int margin);

    wxComboBox *m_cbFreqRange;
    wxComboBox *m_cbMcs;
    wxComboBox *m_cbFixedRefChannel;
    wxComboBox *m_cbDuplexMode;

    wxStaticText *frequenceRangeLabel;
    wxStaticText *mcsLabel;
    wxStaticText *fixedReferenceChannelLabel;
    wxStaticText *channelBandwidthLabel;
    wxStaticText *duplexModeLabel;
    wxStaticText *puschLocationLabel;
    wxStaticText *rbOffsetLabel;
    wxStaticText *subframesLabel;
    wxStaticText *cellIdentityLabel;
    wxStaticText *rntiLabel;
    wxStaticText *windowingSourceLabel;
    wxStaticText *windowingLabel;
    wxStaticText *sampleRateSourceLabel;
};