#include "Frame4.h"

Frame4::Frame4(const wxString &title, const wxPoint &pos, const wxSize &size)
    : wxFrame(NULL, wxID_ANY, title, pos, size)
{
    // Create a main panel
    mainPanel = new wxPanel(this);
    mainSizer = new wxBoxSizer(wxVERTICAL); // Vertical sizer

    // Create an upper panel
    upperPanel = new wxPanel(mainPanel);
    upperPanelSizer = new wxBoxSizer(wxHORIZONTAL); // Horizontal sizer

    // Create an lower panel
    lowerPanel = new wxPanel(mainPanel);

    // Create a button
    wxButton *myButton = new wxButton(lowerPanel, wxID_ANY, "RUN TEST");

    myButton->SetForegroundColour(wxColour(255, 255, 255)); // White
    myButton->SetBackgroundColour(wxColour(0, 0, 255)); // Blue

    // Create a static text control with green text
    wxStaticText *myText = new wxStaticText(lowerPanel, wxID_ANY, "Status : PASSED", wxDefaultPosition, wxDefaultSize);
    myText->SetForegroundColour(wxColour(0, 255, 0)); // Green text color

    // Create a sizer to arrange the controls
    lowerPanelSizer = new wxBoxSizer(wxHORIZONTAL);

    // Add the button and static text to the sizer
    // lowerPanelSizer->Add(myButton, 1, wxALIGN_CENTER | wxALL, 10); // Button
    lowerPanelSizer->Add(myButton, 1, wxALL, 4); // Button
    // lowerPanelSizer->Add(myText, 2, wxALIGN_CENTER | wxALL, 10);   // Static Text
    lowerPanelSizer->Add(myText, 10, wxALL, 10); // Static Text

    // Set the sizer for the main panel
    lowerPanel->SetSizer(lowerPanelSizer);

    // Create the left panel (20%)
    leftUpperPanel = new wxPanel(upperPanel);
    leftSizer = new wxBoxSizer(wxVERTICAL);
    leftUpperPanel->SetBackgroundColour(*wxLIGHT_GREY);

    // Create the right panel (80%)
    rightUpperPanel = new wxPanel(upperPanel);
    rightSizer = new wxBoxSizer(wxVERTICAL);

    descriptionCtrl = new wxTextCtrl(rightUpperPanel, wxID_ANY, "", wxDefaultPosition, wxDefaultSize, wxTE_MULTILINE | wxTE_READONLY | wxVSCROLL);
    descriptionCtrl->SetBackgroundColour(*wxLIGHT_GREY);
    rightSizer->Add(descriptionCtrl, 4, wxEXPAND);

    rightUpperPanel->SetBackgroundColour(*wxWHITE); // Blue background
    rightUpperPanel->SetSizer(rightSizer);

    // Add contents to left and right panels (e.g., controls, widgets)
    treeCtrl = new wxTreeCtrl(leftUpperPanel, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTR_DEFAULT_STYLE);

    wxTreeItemId rootItem4G = treeCtrl->AddRoot("4G");
    wxTreeItemId subItem4G_1 = treeCtrl->AppendItem(rootItem4G, "1)");
    treeCtrl->AppendItem(subItem4G_1, "1.1)");
    treeCtrl->AppendItem(subItem4G_1, "1.2)");

    wxTreeItemId subItem4G_2 = treeCtrl->AppendItem(rootItem4G, "2)");
    treeCtrl->AppendItem(subItem4G_2, "2.1)");
    treeCtrl->AppendItem(subItem4G_2, "2.2)");

    treeCtrl->Expand(rootItem4G);

    treeCtrl1 = new wxTreeCtrl(leftUpperPanel, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTR_DEFAULT_STYLE);
    wxTreeItemId rootItem5G = treeCtrl1->AddRoot("5G");
    wxTreeItemId subItem5G_1 = treeCtrl1->AppendItem(rootItem5G, "1)");
    treeCtrl1->AppendItem(subItem5G_1, "1.1)");
    treeCtrl1->AppendItem(subItem5G_1, "1.2)");

    wxTreeItemId subItem5G_2 = treeCtrl1->AppendItem(rootItem5G, "2)");
    treeCtrl1->AppendItem(subItem5G_2, "2.1)");
    treeCtrl1->AppendItem(subItem5G_2, "2.2)");

    treeCtrl1->Expand(rootItem5G);

    treeCtrl->Bind(wxEVT_TREE_SEL_CHANGED, &Frame4::OnTreeItemSelect, this);
    treeCtrl1->Bind(wxEVT_TREE_SEL_CHANGED, &Frame4::OnTreeItemSelect1, this);

    leftSizer->Add(treeCtrl, 1, wxEXPAND);
    leftSizer->Add(treeCtrl1, 1, wxEXPAND);
    leftUpperPanel->SetSizer(leftSizer);

    // Add the left and right panels to the main sizer with proportions
    upperPanelSizer->Add(leftUpperPanel, 2, wxEXPAND | wxALL, 5);  // 2/10 (20%)
    upperPanelSizer->Add(rightUpperPanel, 8, wxEXPAND | wxALL, 5); // 8/10 (80%)

    // Set the main sizer for the main panel
    upperPanel->SetSizer(upperPanelSizer);

    mainSizer->Add(upperPanel, 8, wxEXPAND | wxALL, 5); // 8/10 (80%)
    mainSizer->Add(lowerPanel, 2, wxEXPAND | wxALL, 5); // 2/10 (20%)

    // Set the main sizer for the main panel
    mainPanel->SetSizer(mainSizer);

    SetSize(1200, 800);
}

void Frame4::OnTreeItemSelect(wxTreeEvent &event)
{
    wxTreeItemId itemId = event.GetItem();
    if (itemId.IsOk())
    {
        wxString text = treeCtrl->GetItemText(itemId);
        descriptionCtrl->Clear();
        for (int i = 1; i <= 100; i++)
        {
            descriptionCtrl->AppendText("[" + text + "] : " + wxString::Format("%d", i) + "\n");
        }
    }
}

void Frame4::OnTreeItemSelect1(wxTreeEvent &event)
{
    wxTreeItemId itemId = event.GetItem();
    if (itemId.IsOk())
    {
        wxString text = treeCtrl1->GetItemText(itemId);
        descriptionCtrl->Clear();
        for (int i = 1; i <= 100; i++)
        {
            descriptionCtrl->AppendText("[" + text + "] : " + wxString::Format("%d", i) + "\n");
        }
    }
}