#pragma once
#include <wx/wx.h>

class IMiddlePanel
{
public:
    virtual void OnDownlinkClick(wxCommandEvent &event) = 0;
    virtual void OnUplinkClick(wxCommandEvent &event) = 0;
    virtual void OnNRTestModelsClick(wxCommandEvent &event) = 0;
    virtual void OnDownlinkFRCClick(wxCommandEvent &event) = 0;
    virtual void OnUplinkFRCClick(wxCommandEvent &event) = 0;
};