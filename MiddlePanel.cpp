#include "MiddlePanel.h"

void MiddlePanel::Scenario1()
{
    const auto margin = FromDIP(3);

    uplinkFRCPanel = new UplinkFRCPanel(this);
    downlinkPanel = new DownlinkPanel(this);

    uplinkFRCPanel->ShowContentUplinkFRC(false);
    downlinkPanel->ShowContentDownlink(false);

    verticalSizer = new wxBoxSizer(wxVERTICAL);
    verticalSizer->Add(downlinkPanel, 0, wxEXPAND | wxALL, margin);
    verticalSizer->Add(uplinkFRCPanel, 0, wxEXPAND | wxALL, margin);

    SetBackgroundColour(wxColour(120, 120, 120)); // Set background color to dark gray

    //SetSizerAndFit(verticalSizer);
    //SetSize(800, 600);
    //SetSize(500, 400);
    SetSizerAndFit(verticalSizer);

    downlinkPanel->Hide();
    uplinkFRCPanel->Hide();
}

void MiddlePanel::OnDownlinkClick(wxCommandEvent &event)
{
    downlinkPanel->Show();
    downlinkPanel->ShowContentDownlink();
    uplinkFRCPanel->Hide();

    Layout();
}

void MiddlePanel::OnUplinkFRCClick(wxCommandEvent &event)
{
    downlinkPanel->Hide();
    uplinkFRCPanel->Show();
    uplinkFRCPanel->ShowContentUplinkFRC();

    Layout();
}

void MiddlePanel::OnUplinkClick(wxCommandEvent &event)
{

}

void MiddlePanel::OnNRTestModelsClick(wxCommandEvent &event)
{

}

void MiddlePanel::OnDownlinkFRCClick(wxCommandEvent &event)
{

}

