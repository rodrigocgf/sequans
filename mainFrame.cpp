///////////////////////////////////////////////////////////////////////////
// C++ code generated with wxFormBuilder (version 3.10.1-376-g11d0e73)
// http://www.wxformbuilder.org/
//
// PLEASE DO *NOT* EDIT THIS FILE!
///////////////////////////////////////////////////////////////////////////

#include "mainFrame.h"

///////////////////////////////////////////////////////////////////////////

MainFrame::MainFrame( wxWindow* parent, wxWindowID id, const wxString& title, const wxPoint& pos, const wxSize& size, long style ) : wxFrame( parent, id, title, pos, size, style )
{
	this->SetSizeHints( wxDefaultSize, wxDefaultSize );

	wxGridBagSizer* gbSizer1;
	gbSizer1 = new wxGridBagSizer( 0, 0 );
	gbSizer1->SetFlexibleDirection( wxBOTH );
	gbSizer1->SetNonFlexibleGrowMode( wxFLEX_GROWMODE_SPECIFIED );

	m_staticText1 = new wxStaticText( this, wxID_ANY, wxT("4G and 5G"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText1->Wrap( -1 );
	gbSizer1->Add( m_staticText1, wxGBPosition( 1, 5 ), wxGBSpan( 1, 1 ), wxALL, 5 );

	m_staticText2 = new wxStaticText( this, wxID_ANY, wxT("Feature 1:"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText2->Wrap( -1 );
	gbSizer1->Add( m_staticText2, wxGBPosition( 3, 6 ), wxGBSpan( 1, 1 ), wxALL, 5 );

	m_staticText3 = new wxStaticText( this, wxID_ANY, wxT("Feature 2:"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText3->Wrap( -1 );
	gbSizer1->Add( m_staticText3, wxGBPosition( 4, 6 ), wxGBSpan( 1, 1 ), wxALL, 5 );

	m_Feature2 = new wxTextCtrl( this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	#ifdef __WXGTK__
	if ( !m_Feature2->HasFlag( wxTE_MULTILINE ) )
	{
	m_Feature2->SetMaxLength( 50 );
	}
	#else
	m_Feature2->SetMaxLength( 50 );
	#endif
	gbSizer1->Add( m_Feature2, wxGBPosition( 4, 7 ), wxGBSpan( 1, 1 ), wxALL, 5 );

	m_button1 = new wxButton( this, wxID_ANY, wxT("ADD FEATURES"), wxDefaultPosition, wxDefaultSize, 0 );
	gbSizer1->Add( m_button1, wxGBPosition( 5, 8 ), wxGBSpan( 1, 1 ), wxALL, 5 );

	m_dataViewListCtrl2 = new wxDataViewListCtrl( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxDV_ROW_LINES );
	gbSizer1->Add( m_dataViewListCtrl2, wxGBPosition( 7, 5 ), wxGBSpan( 2, 2 ), wxALL, 5 );

	m_button2 = new wxButton( this, wxID_ANY, wxT("Exit"), wxDefaultPosition, wxDefaultSize, 0 );
	gbSizer1->Add( m_button2, wxGBPosition( 20, 35 ), wxGBSpan( 1, 1 ), wxALL, 5 );

	m_Feature1 = new wxTextCtrl( this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	#ifdef __WXGTK__
	if ( !m_Feature1->HasFlag( wxTE_MULTILINE ) )
	{
	m_Feature1->SetMaxLength( 50 );
	}
	#else
	m_Feature1->SetMaxLength( 50 );
	#endif
	gbSizer1->Add( m_Feature1, wxGBPosition( 3, 7 ), wxGBSpan( 1, 1 ), wxALL, 5 );


	this->SetSizer( gbSizer1 );
	this->Layout();

	this->Centre( wxBOTH );

	// Connect Events
	m_button1->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( MainFrame::OnAddFeaturesClick ), NULL, this );
	m_button2->Connect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( MainFrame::OnExitClick ), NULL, this );
}

MainFrame::~MainFrame()
{
	// Disconnect Events
	m_button1->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( MainFrame::OnAddFeaturesClick ), NULL, this );
	m_button2->Disconnect( wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler( MainFrame::OnExitClick ), NULL, this );

}
void MainFrame::OnAddFeaturesClick(wxCommandEvent &event)
{
	// wxMessageBox("This is a wxWidgets SEQUANS Application!",
	// 			 "4G -> 5G", wxOK | wxICON_INFORMATION);
}

void MainFrame::OnExitClick(wxCommandEvent &event)
{
	Close(true);
}
