#include "WaveformPanel.h"


void WaveformPanel::OnDownlinkClick(wxCommandEvent &event)
{
    m_pMiddlePanel->OnDownlinkClick(event);
}

void WaveformPanel::OnUplinkClick(wxCommandEvent &event)
{
    m_pMiddlePanel->OnUplinkClick(event);
}

void WaveformPanel::OnNRTestModelsClick(wxCommandEvent &event)
{
    m_pMiddlePanel->OnNRTestModelsClick(event);
    // wxMessageBox("OnNRTestModelsClick Clicked!", "Info", wxOK | wxICON_INFORMATION);
}

void WaveformPanel::OnDownlinkFRCClick(wxCommandEvent &event)
{
    m_pMiddlePanel->OnDownlinkFRCClick(event);
    // wxMessageBox("DownlinkFRC Clicked!", "Info", wxOK | wxICON_INFORMATION);
}

void WaveformPanel::OnUplinkFRCClick(wxCommandEvent &event)
{
    //wxMessageBox("UplinkFRC Clicked!", "Info", wxOK | wxICON_INFORMATION);
    m_pMiddlePanel->OnUplinkFRCClick(event);
    
}

void WaveformPanel::Create()
{
    Scenario2();
}

void WaveformPanel::Scenario1()
{
    const auto margin = FromDIP(3);
    wxStaticBox *staticBox = new wxStaticBox(this, wxID_ANY, "");
    staticBox->SetFont(wxFont(wxFontInfo(12).Bold()));
    wxStaticBoxSizer *staticBoxSizer = new wxStaticBoxSizer(staticBox, wxHORIZONTAL);

    wxGridBagSizer *sizer = new wxGridBagSizer(margin, margin);
    sizer->SetFlexibleDirection(wxBOTH);
    sizer->SetNonFlexibleGrowMode(wxFLEX_GROWMODE_SPECIFIED);

    btnDownlink = nullptr;
    btnDownlink = new wxToggleButton(this, wxID_ANY, wxT(""), wxDefaultPosition, wxDefaultSize, margin);

    btnUplink = nullptr;
    btnUplink = new wxToggleButton(this, wxID_ANY, wxT(""), wxDefaultPosition, wxDefaultSize, margin);

    btnNRTestModels = nullptr;
    btnNRTestModels = new wxToggleButton(this, wxID_ANY, wxT(""), wxDefaultPosition, wxDefaultSize, margin);

    btnDownlinkFRC = nullptr;
    btnDownlinkFRC = new wxToggleButton(this, wxID_ANY, wxT(""), wxDefaultPosition, wxDefaultSize, margin);

    btnUplinkFRC = nullptr;
    btnUplinkFRC = new wxToggleButton(this, wxID_ANY, wxT(""), wxDefaultPosition, wxDefaultSize, margin);

    btnDownlink->SetBitmap(wxBitmap(wxT("./images/Downlink_1.png"), wxBITMAP_TYPE_ANY));
    btnDownlink->SetSize(30, 30);
    btnDownlink->Bind(wxEVT_BUTTON, &WaveformPanel::OnDownlinkClick, this);

    btnUplink->SetBitmap(wxBitmap(wxT("./images/Uplink_1.png"), wxBITMAP_TYPE_ANY));
    btnUplink->SetSize(30, 30);
    btnUplink->Bind(wxEVT_BUTTON, &WaveformPanel::OnUplinkClick, this);

    btnNRTestModels->SetBitmap(wxBitmap(wxT("./images/NR_test_models_1.png"), wxBITMAP_TYPE_ANY));
    btnNRTestModels->SetSize(30, 30);
    btnNRTestModels->Bind(wxEVT_BUTTON, &WaveformPanel::OnNRTestModelsClick, this);

    btnDownlinkFRC->SetBitmap(wxBitmap(wxT("./images/Downlink_FRC_1.png"), wxBITMAP_TYPE_ANY));
    btnDownlinkFRC->SetSize(30, 30);
    btnDownlinkFRC->Bind(wxEVT_BUTTON, &WaveformPanel::OnDownlinkFRCClick, this);

    btnUplinkFRC->SetBitmap(wxBitmap(wxT("./images/Uplink_FRC_1.png"), wxBITMAP_TYPE_ANY));
    btnUplinkFRC->SetSize(30, 30);
    btnUplinkFRC->Bind(wxEVT_BUTTON, &WaveformPanel::OnUplinkFRCClick, this);

    wxStaticText *waveFormType = new wxStaticText(this, wxID_ANY, "WAVE FORM TYPE");

    sizer->Add(btnDownlink, {1, 1}, {2, 1}, wxALIGN_RIGHT);
    sizer->Add(btnUplink, {1, 2}, {2, 1}, wxALIGN_RIGHT);
    sizer->Add(btnNRTestModels, {1, 3}, {2, 1}, wxALIGN_RIGHT);
    sizer->Add(btnDownlinkFRC, {1, 4}, {2, 1}, wxALIGN_RIGHT);
    sizer->Add(btnUplinkFRC, {1, 5}, {2, 1}, wxALIGN_RIGHT);
    sizer->Add(waveFormType, {3, 2}, {1, 3}, wxALIGN_CENTER);

    staticBoxSizer->Add(sizer, wxSizerFlags().Border(wxALL, 5));

    SetSizerAndFit(staticBoxSizer);

    // wxMessageBox("WaveformPanel", "Info", wxOK | wxICON_INFORMATION);
}

void WaveformPanel::Scenario2()
{
    const auto margin = FromDIP(3);

    wxGridBagSizer *sizer = new wxGridBagSizer(margin, margin);
    //sizer->SetFlexibleDirection(wxBOTH);
    //sizer->SetNonFlexibleGrowMode(wxFLEX_GROWMODE_SPECIFIED);

    btnDownlink = nullptr;
    btnDownlink = new wxToggleButton(this, wxID_ANY, wxT(""), wxDefaultPosition, wxDefaultSize, 0);

    btnUplink = nullptr;
    btnUplink = new wxToggleButton(this, wxID_ANY, wxT(""), wxDefaultPosition, wxDefaultSize, 0);

    btnNRTestModels = nullptr;
    btnNRTestModels = new wxToggleButton(this, wxID_ANY, wxT(""), wxDefaultPosition, wxDefaultSize, 0);

    btnDownlinkFRC = nullptr;
    btnDownlinkFRC = new wxToggleButton(this, wxID_ANY, wxT(""), wxDefaultPosition, wxDefaultSize, 0);

    btnUplinkFRC = nullptr;
    btnUplinkFRC = new wxToggleButton(this, wxID_ANY, wxT(""), wxDefaultPosition, wxDefaultSize, 0);

    btnDownlink->SetBitmap(wxBitmap(wxT("./images/Downlink_1.png"), wxBITMAP_TYPE_ANY));
    btnDownlink->SetSize(30, 30);
    btnDownlink->Bind(wxEVT_TOGGLEBUTTON, &WaveformPanel::OnDownlinkClick, this);

    btnUplink->SetBitmap(wxBitmap(wxT("./images/Uplink_1.png"), wxBITMAP_TYPE_ANY));
    btnUplink->SetSize(30, 30);
    btnUplink->Bind(wxEVT_TOGGLEBUTTON, &WaveformPanel::OnUplinkClick, this);

    btnNRTestModels->SetBitmap(wxBitmap(wxT("./images/NR_test_models_1.png"), wxBITMAP_TYPE_ANY));
    btnNRTestModels->SetSize(30, 30);
    btnNRTestModels->Bind(wxEVT_TOGGLEBUTTON, &WaveformPanel::OnNRTestModelsClick, this);

    btnDownlinkFRC->SetBitmap(wxBitmap(wxT("./images/Downlink_FRC_1.png"), wxBITMAP_TYPE_ANY));
    btnDownlinkFRC->SetSize(30, 30);
    btnDownlinkFRC->Bind(wxEVT_TOGGLEBUTTON, &WaveformPanel::OnDownlinkFRCClick, this);

    btnUplinkFRC->SetBitmap(wxBitmap(wxT("./images/Uplink_FRC_1.png"), wxBITMAP_TYPE_ANY));
    btnUplinkFRC->SetSize(30, 30);
    btnUplinkFRC->Bind(wxEVT_TOGGLEBUTTON, &WaveformPanel::OnUplinkFRCClick, this);

    wxStaticText *waveFormType = new wxStaticText(this, wxID_ANY, "WAVE FORM TYPE");

    sizer->Add(btnDownlink, {1, 1}, {2, 1}, wxALIGN_RIGHT);
    sizer->Add(btnUplink, {1, 2}, {2, 1}, wxALIGN_RIGHT);
    sizer->Add(btnNRTestModels, {1, 3}, {2, 1}, wxALIGN_RIGHT);
    sizer->Add(btnDownlinkFRC, {1, 4}, {2, 1}, wxALIGN_RIGHT);
    sizer->Add(btnUplinkFRC, {1, 5}, {2, 1}, wxALIGN_RIGHT);
    sizer->Add(waveFormType, {3, 2}, {1, 3}, wxALIGN_CENTER);

    SetBackgroundColour(wxColour(190, 190, 50));

    SetSizerAndFit(sizer);

    // wxMessageBox("WaveformPanel", "Info", wxOK | wxICON_INFORMATION);
}