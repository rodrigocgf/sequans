#include "Frame2.h"

Frame2::Frame2(const wxString &title, const wxPoint &pos, const wxSize &size)
    : wxFrame(NULL, wxID_ANY, title, pos, size)
{

    // Create a top-level sizer for the frame
    wxPanel *mainPanel = new wxPanel(this);
    mainSizer = new wxBoxSizer(wxVERTICAL);

    // Create a splitter window for the upper panel
    wxSplitterWindow *splitterUpper = new wxSplitterWindow(this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxSP_LIVE_UPDATE);
    splitterUpper->SetSashGravity(0.5); // Split the upper panel evenly

    // Create the left panel for the list
    leftUpperPanel = new wxPanel(splitterUpper, wxID_ANY);
    leftSizer = new wxBoxSizer(wxVERTICAL);

    treeCtrl = new wxTreeCtrl(leftUpperPanel, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTR_DEFAULT_STYLE);

    wxTreeItemId rootItem4G = treeCtrl->AddRoot("4G");
    wxTreeItemId subItem4G_1 = treeCtrl->AppendItem(rootItem4G, "1)");
    treeCtrl->AppendItem(subItem4G_1, "1.1)");
    treeCtrl->AppendItem(subItem4G_1, "1.2)");

    wxTreeItemId subItem4G_2 = treeCtrl->AppendItem(rootItem4G, "2)");
    treeCtrl->AppendItem(subItem4G_2, "2.1)");
    treeCtrl->AppendItem(subItem4G_2, "2.2)");

    treeCtrl->Expand(rootItem4G);

    treeCtrl1 = new wxTreeCtrl(leftUpperPanel, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTR_DEFAULT_STYLE);
    wxTreeItemId rootItem5G = treeCtrl1->AddRoot("5G");
    wxTreeItemId subItem5G_1 = treeCtrl1->AppendItem(rootItem5G, "1)");
    treeCtrl1->AppendItem(subItem5G_1, "1.1)");
    treeCtrl1->AppendItem(subItem5G_1, "1.2)");

    wxTreeItemId subItem5G_2 = treeCtrl1->AppendItem(rootItem5G, "2)");
    treeCtrl1->AppendItem(subItem5G_2, "2.1)");
    treeCtrl1->AppendItem(subItem5G_2, "2.2)");

    treeCtrl1->Expand(rootItem5G);

    treeCtrl->Bind(wxEVT_TREE_SEL_CHANGED, &Frame2::OnTreeItemSelect, this);
    treeCtrl1->Bind(wxEVT_TREE_SEL_CHANGED, &Frame2::OnTreeItemSelect1, this);

    // Add items and subitems to the listCtrl here
    leftSizer->Add(treeCtrl, 1, wxEXPAND);
    leftSizer->Add(treeCtrl1, 1, wxEXPAND);
    leftUpperPanel->SetSizer(leftSizer);

    // Create the right panel for the description
    rightUpperPanel = new wxPanel(splitterUpper, wxID_ANY);
    rightSizer = new wxBoxSizer(wxVERTICAL);

    descriptionCtrl = new wxTextCtrl(rightUpperPanel, wxID_ANY, "", wxDefaultPosition, wxDefaultSize, wxTE_MULTILINE | wxTE_READONLY | wxVSCROLL);
    descriptionCtrl->SetBackgroundColour(*wxLIGHT_GREY);
    rightSizer->Add(descriptionCtrl, 4, wxEXPAND);

    rightUpperPanel->SetSizer(rightSizer);

    // Set the left and right panels in the splitterUpper window
    splitterUpper->SplitVertically(leftUpperPanel, rightUpperPanel);

    // Create a sizer for the upper panels
    wxBoxSizer *upperPanelSizer = new wxBoxSizer(wxHORIZONTAL);
    upperPanelSizer->Add(leftUpperPanel, 1, wxEXPAND);
    upperPanelSizer->Add(rightUpperPanel, 4, wxEXPAND);

    // // Set the size of the upper left panel (30% of the upper panel width)
    // int upperPanelWidth = size.GetWidth();
    // int upperLeftPanelWidth = 0.3 * upperPanelWidth;
    // leftUpperPanel->SetSize(0, 0, upperLeftPanelWidth, size.GetHeight());

    // splitterUpper->UpdateSize();

    splitterUpper->SetSizer(upperPanelSizer);

    // Create the bottom panel
    wxPanel *bottomPanel = new wxPanel(this);
    descriptionBotton = new wxTextCtrl(bottomPanel, wxID_ANY, "Test status : PASSED", wxDefaultPosition, wxDefaultSize, wxTE_MULTILINE | wxTE_READONLY | wxVSCROLL);
    descriptionBotton->SetBackgroundColour(*wxYELLOW);
    bottonSizer = new wxBoxSizer(wxHORIZONTAL);
    bottonSizer->Add(descriptionBotton, 1, wxEXPAND);
    bottomPanel->SetBackgroundColour(*wxLIGHT_GREY);
    bottomPanel->SetSizer(bottonSizer);

    // Add the bottom panel to the frame's sizer
    mainSizer->Add(splitterUpper, 5, wxEXPAND);
    mainSizer->Add(bottomPanel, 1, wxEXPAND);

    SetSizerAndFit(mainSizer);

    SetSize(1200, 800);

    // Set up menu and other event handling here
}

void Frame2::OnTreeItemSelect(wxTreeEvent &event)
{
    wxTreeItemId itemId = event.GetItem();
    if (itemId.IsOk())
    {
        wxString text = treeCtrl->GetItemText(itemId);
        descriptionCtrl->Clear();
        for (int i = 1; i <= 100; i++)
        {
            descriptionCtrl->AppendText("[" + text + "] : " + wxString::Format("%d", i) + "\n");
        }
    }
}

void Frame2::OnTreeItemSelect1(wxTreeEvent &event)
{
    wxTreeItemId itemId = event.GetItem();
    if (itemId.IsOk())
    {
        wxString text = treeCtrl1->GetItemText(itemId);
        descriptionCtrl->Clear();
        for (int i = 1; i <= 100; i++)
        {
            descriptionCtrl->AppendText("[" + text + "] : " + wxString::Format("%d", i) + "\n");
        }
    }
}