#pragma once

#include <wx/wx.h>
#include <wx/dcclient.h>
#include <wx/sizer.h>
#include <wx/dcclient.h>
#include <iostream>
#include <string>
#include <cmath>

class GraphPanel : public wxPanel
{
    double v(double t, double A, double w0);

public:

    GraphPanel(wxWindow *parent,  wxWindowID id = wxID_ANY,
               const wxPoint &pos = wxDefaultPosition, const wxSize &size = wxDefaultSize)
        : wxPanel(parent, id, pos, wxSize(800, 300))
    {

        Layout();
    }

private:
    void OnPaint(wxPaintEvent &event);
    void DrawGraph(wxDC &dc);

    double TriangularWave(double t);
    double SinusoidWave(double t);
    double SquareWave(double t);

    DECLARE_EVENT_TABLE()
};

