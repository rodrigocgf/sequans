#pragma once

#include <sqlite3.h>
#include <iostream>
#include <string>

class SQLite
{
public:
    SQLite(const std::string &dbFileName) : dbName(dbFileName), db(nullptr) {}

    // Open the database
    bool open();

    // Close the database
    void close();

    // Execute an SQL query (Create, Update, Delete)
    bool executeQuery(const std::string &query);

    // Execute a SELECT query
    bool executeSelect(const std::string &query, std::string &result);

private:
    std::string dbName;
    sqlite3 *db;
};