#include "Frame9.h"

Frame9::Frame9(const wxString &title, const wxPoint &pos, const wxSize &size)
    : wxFrame(NULL, wxID_ANY, title, pos, size)
{
    Scenario1();
    // Center the frame on the screen
    Centre();
}

void Frame9::Scenario1()
{
    UpperLeftPanel *upperPanel = new UpperLeftPanel(this, wxID_ANY, wxDefaultPosition, wxSize(-1, 300));
    //MiddlePanel *middlePanel = new MiddlePanel(this, wxID_ANY, wxDefaultPosition, wxSize(-1, 200));
    MiddlePanel *middlePanel = new MiddlePanel(this, wxID_ANY);
    BottonPanel *bottonPanel = new BottonPanel(this, wxID_ANY, wxDefaultPosition, wxSize(-1, 400));
    WaveformPanel *waveformPanel = new WaveformPanel(this, middlePanel);

    std::function<double(double)> f_TriangularWave = &Frame9::TriangularWave;
    std::function<double(double)> f_SinusoidWave = &Frame9::SinusoidWave;
    std::function<double(double)> f_SquareWave = &Frame9::SquareWave;
    GraphPanel<Time, 200, 150> *graphPanel = new GraphPanel<Time, 200, 150>(this, f_SquareWave);
    // GraphPanel<Frequency, 400, 300> *graphPanel1 = new GraphPanel<Frequency, 400, 300>(this, f_SinusoidWave);
    // GraphPanel<Time, 400, 300> *graphPanel1 = new GraphPanel<Time, 400, 300>(this, f_SinusoidWave);
    GraphPanel<Time, 200, 150> *graphPanel1 = new GraphPanel<Time, 200, 150>(this, f_TriangularWave);

    // Set up sizer for the frame
    wxBoxSizer *verticalSizer = new wxBoxSizer(wxVERTICAL);

    wxBoxSizer *horizontalUpperSizer = new wxBoxSizer(wxHORIZONTAL);
    horizontalUpperSizer->Add(upperPanel, 0,wxEXPAND);
    horizontalUpperSizer->Add(waveformPanel, 0, wxEXPAND);

    wxBoxSizer *horizontalMiddleSizer = new wxBoxSizer(wxHORIZONTAL);
    horizontalMiddleSizer->Add(graphPanel, wxSizerFlags().Expand().Border(wxALL, 10));
    horizontalMiddleSizer->Add(graphPanel1, wxSizerFlags().Expand().Border(wxALL, 10));

    verticalSizer->Add(horizontalUpperSizer, 0, wxEXPAND);
    verticalSizer->Add(horizontalMiddleSizer, 0, wxEXPAND);

    verticalSizer->Add(middlePanel, 1, wxEXPAND);
    verticalSizer->Add(bottonPanel, 0, wxEXPAND);

    SetSizerAndFit(verticalSizer);
}

void Frame9::Scenario2()
{
    UpperLeftPanel *upperPanel = new UpperLeftPanel(this);
    MiddlePanel *middlePanel = new MiddlePanel(this);
    BottonPanel *bottonPanel = new BottonPanel(this);
    WaveformPanel *waveformPanel = new WaveformPanel(this, middlePanel);


    // Set up sizer for the frame
    wxBoxSizer *verticalSizer = new wxBoxSizer(wxVERTICAL);

    wxBoxSizer *horizontalUpperSizer = new wxBoxSizer(wxHORIZONTAL);
    horizontalUpperSizer->Add(upperPanel, wxSizerFlags().Expand().Border(wxALL, 10));
    horizontalUpperSizer->Add(waveformPanel, wxSizerFlags().Expand().Border(wxALL, 10));

    verticalSizer->Add(horizontalUpperSizer, wxSizerFlags().Expand().Border(wxALL, 10));

    verticalSizer->Add(middlePanel, wxSizerFlags().Expand().Border(wxALL, 10));
    verticalSizer->Add(bottonPanel, wxSizerFlags().Expand().Border(wxALL, 10));

    SetSizerAndFit(verticalSizer);
}

double Frame9::SinusoidWave(double t)
{
    double Amplitude = 200.0;
    double w0 = 0.4;

    return Amplitude * cos(w0 * t);
}

double Frame9::SinusoidWaveAlt(double t)
{
    double Amplitude = 200.0;
    double w0 = 2.0;
    double pi = 3.14159265358979323846;
    return Amplitude / 4 + Amplitude * std::sqrt(2) / pi * std::cos(w0 * t);
}

double Frame9::TriangularWave(double t)
{
    double Amplitude = 100.0;
    double w0 = 0.5;
    double result = 0.0;
    for (int n = 1; n <= 200; n += 2)
    {
        result += (4.0 / (n * M_PI)) * sin(w0 * (2.0 * n - 1) * t) / (2.0 * n - 1);
    }

    return Amplitude * result;
}

double Frame9::SquareWave(double t)
{
    double Amplitude = 200.0;
    double w0 = 0.4;
    double d = 0.5;
    double result = 0.0;
    for (int n = 1; n <= 50; n += 2)
    {
        result += (2.0 / (n * M_PI)) * sin(M_PI * n * d) * cos(w0 * n * t);
    }

    return Amplitude * result;
}