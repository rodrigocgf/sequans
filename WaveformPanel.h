#pragma once

#include <wx/wx.h>
#include <wx/gbsizer.h>
#include <wx/image.h>
#include <wx/panel.h>
#include <wx/sizer.h>
#include <wx/tglbtn.h>
#include "IMiddlePanel.h"

class WaveformPanel : public wxPanel
{
public:
    WaveformPanel(wxWindow *parent, IMiddlePanel *pMiddlePanel, wxWindowID id = wxID_ANY, const wxPoint &pos = wxDefaultPosition, const wxSize &size = wxDefaultSize)
        : wxPanel(parent, id, pos, size), m_pMiddlePanel(pMiddlePanel)
    {
        wxInitAllImageHandlers();
        
        Create();
    }

private:
    enum
    {
        WaveformPanel_Downlink = wxID_HIGHEST,
        WaveformPanel_Uplink,
        WaveformPanel_NRTestModels,
        WaveformPanel_UplinkFRC,
        WaveformPanel_DownlinkFRC
    };

    IMiddlePanel *m_pMiddlePanel;
    wxToggleButton *btnDownlink;
    wxToggleButton *btnUplink;
    wxToggleButton *btnNRTestModels;
    wxToggleButton *btnDownlinkFRC;
    wxToggleButton *btnUplinkFRC;

    void OnDownlinkClick(wxCommandEvent &event);

    void OnUplinkClick(wxCommandEvent &event);

    void OnNRTestModelsClick(wxCommandEvent &event);

    void OnDownlinkFRCClick(wxCommandEvent &event);

    void OnUplinkFRCClick(wxCommandEvent &event);

    void Create();

    void Scenario1();

    void Scenario2();

};