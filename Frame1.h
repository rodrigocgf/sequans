#pragma once

#include <wx/wx.h>

#include <wx/splitter.h>
#include <wx/textctrl.h>
#include <wx/listctrl.h>
#include <wx/treectrl.h>

class Frame1 : public wxFrame
{
public:
    Frame1(const wxString &title, const wxPoint &pos, const wxSize &size);

    void OnTreeItemSelect(wxTreeEvent &event);

    void OnTreeItemSelect1(wxTreeEvent &event);

private:
    wxTreeCtrl *treeCtrl;
    wxTreeCtrl *treeCtrl1;

    wxPanel *rightPanel;
    wxBoxSizer *rightSizer;
    wxTextCtrl *descriptionCtrl;

    wxPanel *leftPanel;
    wxBoxSizer *leftSizer;
};