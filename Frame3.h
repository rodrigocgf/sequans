#pragma once

#include <wx/wx.h>

#include <wx/splitter.h>
#include <wx/textctrl.h>
#include <wx/listctrl.h>
#include <wx/treectrl.h>

class Frame3 : public wxFrame
{
public:
    Frame3(const wxString &title, const wxPoint &pos, const wxSize &size);

    void OnTreeItemSelect(wxTreeEvent &event);

    void OnTreeItemSelect1(wxTreeEvent &event);

private:
    wxPanel *mainPanel;
    wxBoxSizer *mainSizer;

    wxTreeCtrl *treeCtrl;
    wxTreeCtrl *treeCtrl1;
    wxTextCtrl *descriptionCtrl;

    wxPanel *rightPanel;
    wxBoxSizer *rightSizer;

    wxPanel *leftPanel;
    wxBoxSizer *leftSizer;
};