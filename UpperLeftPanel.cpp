#include "UpperLeftPanel.h"

void UpperLeftPanel::Scenario1()
{
    // wxStaticBox *staticBox = new wxStaticBox(this, wxID_ANY, "Panel Border");
    const auto margin = FromDIP(5);

    wxToggleButton *btnNewSession = new wxToggleButton(this, wxID_ANY, wxT(""), wxDefaultPosition, wxDefaultSize, 0);
    wxToggleButton *btnOpenSession = new wxToggleButton(this, wxID_ANY, wxT(""), wxDefaultPosition, wxDefaultSize, 0);
    wxToggleButton *btnSaveSession = new wxToggleButton(this, wxID_ANY, wxT(""), wxDefaultPosition, wxDefaultSize, 0);

    btnNewSession->SetBitmap(wxBitmap(wxT("./images/New_session.png"), wxBITMAP_TYPE_ANY));
    btnNewSession->SetSize(30, 30);
    btnNewSession->Bind(wxEVT_TOGGLEBUTTON, &UpperLeftPanel::OnNewSession, this);

    btnOpenSession->SetBitmap(wxBitmap(wxT("./images/Open_session.png"), wxBITMAP_TYPE_ANY));
    btnOpenSession->SetSize(30, 30);
    btnOpenSession->Bind(wxEVT_TOGGLEBUTTON, &UpperLeftPanel::OnOpenSession, this);

    btnSaveSession->SetBitmap(wxBitmap(wxT("./images/Save_session.png"), wxBITMAP_TYPE_ANY));
    btnSaveSession->SetSize(30, 30);
    btnSaveSession->Bind(wxEVT_TOGGLEBUTTON, &UpperLeftPanel::OnSaveSession, this);

    // wxStaticBoxSizer *staticBoxSizer = new wxStaticBoxSizer(staticBox, wxHORIZONTAL);
    wxBoxSizer *sizer = new wxBoxSizer(wxHORIZONTAL);
    sizer->Add(btnNewSession, 0, wxEXPAND | wxALL, margin);
    sizer->Add(btnOpenSession, 0, wxEXPAND | wxALL, margin);
    sizer->Add(btnSaveSession, 0, wxEXPAND | wxALL, margin);

    SetBackgroundColour(wxColour(120, 120, 120));

    // Set the sizer for the panel
    SetSizerAndFit(sizer);
}

void UpperLeftPanel::Scenario2()
{
    const auto margin = FromDIP(10);

    wxGridBagSizer *sizer = new wxGridBagSizer(margin, margin);

    wxButton *btnNewSession = new wxButton(this, wxID_ANY, "New Session");
    wxButton *btnOpenSession = new wxButton(this, wxID_ANY, "Open Session");
    wxButton *btnSaveSession = new wxButton(this, wxID_ANY, "Save Session");

    btnNewSession->Bind(wxEVT_BUTTON, &UpperLeftPanel::OnNewSession, this);
    btnOpenSession->Bind(wxEVT_BUTTON, &UpperLeftPanel::OnOpenSession, this);
    btnSaveSession->Bind(wxEVT_BUTTON, &UpperLeftPanel::OnSaveSession, this);

    // btnNewSession->SetSize(wxSize(100, 50));

    sizer->Add(btnNewSession, {1, 1}, {2, 1}, wxALIGN_RIGHT);
    sizer->Add(btnOpenSession, {1, 2}, {2, 1}, wxALIGN_RIGHT);
    sizer->Add(btnSaveSession, {1, 3}, {2, 1}, wxALIGN_RIGHT);

    SetBackgroundColour(wxColour(100, 100, 100)); // Set background color to dark gray

    SetSizerAndFit(sizer);

    //wxMessageBox("UpperPanel", "Info", wxOK | wxICON_INFORMATION); // OK
}

void UpperLeftPanel::OnNewSession(wxCommandEvent &event)
{
    wxMessageBox("New Session", "Info", wxOK | wxICON_INFORMATION);
}

void UpperLeftPanel::OnOpenSession(wxCommandEvent &event)
{
    wxMessageBox("Open Session", "Info", wxOK | wxICON_INFORMATION);
}

void UpperLeftPanel::OnSaveSession(wxCommandEvent &event)
{
    wxMessageBox("Save Session", "Info", wxOK | wxICON_INFORMATION);
}