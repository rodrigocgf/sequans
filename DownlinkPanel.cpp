#include "DownlinkPanel.h"

void DownlinkPanel::Scenario1()
{
    const auto margin = FromDIP(10);

    mainSizer = new wxBoxSizer(wxVERTICAL);
    panelDownlink = new wxPanel(this, wxID_ANY);
    SetBackgroundColour(wxColour(190, 190, 190)); // Set background color to dark gray
    expandedDownlink = false;

    CreateArrowDownlink(panelDownlink);
    CreateLabelsDownlink(panelDownlink);

    arrowLabelDownlinkSizer = new wxGridBagSizer(margin, margin);
    AddElementsToSizerDownlink(margin);
    ShowContentDownlink(true);

    panelDownlink->SetSizer(arrowLabelDownlinkSizer);
    mainSizer->Add(panelDownlink, 1, wxEXPAND | wxALL, margin);
    //SetSizerAndFit(mainSizer);
    //SetSize(800, 300);
    SetSizerAndFit(mainSizer);
}

void DownlinkPanel::OnArrowClickDownlink(wxMouseEvent &event)
{
    wxString label = "Downlink";

    expandedDownlink = !expandedDownlink;
    if (expandedDownlink)
    {
        arrowLabelDownlink->SetLabel(label + "   \u25BC");
    }
    else
    {
        arrowLabelDownlink->SetLabel(label + "   \u25B6");
        panelDownlink->SetSize(1, 1);
    }

    ShowContentDownlink(expandedDownlink);
    Layout();


}

void DownlinkPanel::CreateArrowDownlink(wxPanel *panel)
{
    wxString label = "Downlink";
    arrowLabelDownlink = new wxStaticText(panel, wxID_ANY, label + "   \u25BC", wxDefaultPosition, wxDefaultSize, wxALIGN_RIGHT);

    wxFont font = arrowLabelDownlink->GetFont();
    font.MakeBold();
    arrowLabelDownlink->SetFont(font);
    arrowLabelDownlink->Bind(wxEVT_LEFT_DOWN, &DownlinkPanel::OnArrowClickDownlink, this);
}

void DownlinkPanel::CreateLabelsDownlink(wxPanel *panel)
{

    m_Downlink.label = new wxStaticText(panel, wxID_ANY, "Label: ");
    m_Downlink.frequenceRangeLabel = new wxStaticText(panel, wxID_ANY, "Frequency range: ");
    m_Downlink.channelBandwidthLabel = new wxStaticText(panel, wxID_ANY, "Channel bandwidth (MHz): ");
    m_Downlink.cellIdentityLabel = new wxStaticText(panel, wxID_ANY, "Cell identity: ");
    m_Downlink.subframesLabel = new wxStaticText(panel, wxID_ANY, "Subframes: ");
    m_Downlink.initialSubframeLabel = new wxStaticText(panel, wxID_ANY, "Initial subframe: ");

    m_Downlink.windowingSourceLabel = new wxStaticText(panel, wxID_ANY, "Windowing source: ");
    m_Downlink.windowingLabel = new wxStaticText(panel, wxID_ANY, "Windowing (%): ");
}

void DownlinkPanel::AddElementsToSizerDownlink(const int margin)
{
    contentSizerDownlink = new wxGridBagSizer(margin, margin);
    contentSizerDownlink->Add(m_Downlink.label, {1, 0}, {1, 1}, wxALIGN_RIGHT);
    contentSizerDownlink->Add(m_Downlink.frequenceRangeLabel, {2, 0}, {1, 1}, wxALIGN_RIGHT);
    contentSizerDownlink->Add(m_Downlink.channelBandwidthLabel, {3, 0}, {1, 1}, wxALIGN_RIGHT);
    contentSizerDownlink->Add(m_Downlink.cellIdentityLabel, {4, 0}, {1, 1}, wxALIGN_RIGHT);
    contentSizerDownlink->Add(m_Downlink.subframesLabel, {5, 0}, {1, 1}, wxALIGN_RIGHT);
    contentSizerDownlink->Add(m_Downlink.initialSubframeLabel, {6, 0}, {1, 1}, wxALIGN_RIGHT);

    contentSizerDownlink->Add(m_Downlink.windowingSourceLabel, {7, 0}, {1, 1}, wxALIGN_RIGHT);
    contentSizerDownlink->Add(m_Downlink.windowingLabel, {8, 0}, {1, 1}, wxALIGN_RIGHT);

    arrowLabelDownlinkSizer->Add(arrowLabelDownlink, 0, wxUP, margin);
    arrowLabelDownlinkSizer->Add(contentSizerDownlink, 1, wxDOWN, margin);
}

void DownlinkPanel::ShowContentDownlink(bool expanded)
{
    m_Downlink.label->Show(expanded);
    m_Downlink.frequenceRangeLabel->Show(expanded);
    m_Downlink.channelBandwidthLabel->Show(expanded);
    m_Downlink.cellIdentityLabel->Show(expanded);
    m_Downlink.subframesLabel->Show(expanded);
    m_Downlink.initialSubframeLabel->Show(expanded);
    m_Downlink.windowingSourceLabel->Show(expanded);
    m_Downlink.windowingLabel->Show(expanded);
}