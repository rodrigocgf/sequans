// Start of wxWidgets "Hello World" Program
#include <wx/wx.h>
#include "mainFrame.h"

#include "Frame1.h"
#include "Frame2.h"
#include "Frame3.h"
#include "Frame4.h"
#include "Frame5.h"
#include "Frame6.h"
#include "Frame7.h"
#include "Frame8.h"
#include "Frame9.h"

void FrameMain();
void Frame_1();
void Frame_2();
void Frame_3();
void Frame_4();
void Frame_5();
void Frame_6();
void Frame_7();
void Frame_8();
void Frame_9();

class MyApp : public wxApp
{
public:
    bool OnInit() override;
    
};

wxIMPLEMENT_APP(MyApp);

void FrameMain()
{
    MainFrame *frame = new MainFrame(NULL, wxID_ANY, "SEQUANS");
    frame->Show(true);
    //SetTopWindow(frame);
}

void Frame_1()
{
    Frame1 *frame = new Frame1("SEQUANS 4G/5G", wxDefaultPosition, wxSize(800, 600));
    frame->Show(true);
    // SetTopWindow(frame);
}

void Frame_2()
{
    Frame2 *frame = new Frame2("SEQUANS 4G/5G", wxDefaultPosition, wxSize(800, 600));
    frame->Show(true);
    // SetTopWindow(frame);
}

void Frame_3()
{
    Frame3 *frame = new Frame3("SEQUANS 4G/5G", wxDefaultPosition, wxSize(800, 600));
    frame->Show(true);
    // SetTopWindow(frame);
}

void Frame_4()
{
    Frame4 *frame = new Frame4("SEQUANS 4G/5G", wxDefaultPosition, wxSize(800, 600));
    frame->Show(true);
    // SetTopWindow(frame);
}

void Frame_5()
{
    Frame5 *frame = new Frame5("SEQUANS 4G/5G", wxDefaultPosition, wxSize(800, 600));
    frame->Show(true);
    // SetTopWindow(frame);
}

void Frame_6()
{
    Frame6 *frame = new Frame6("SEQUANS 4G/5G", wxDefaultPosition, wxSize(800, 600));
    frame->Show(true);
    // SetTopWindow(frame);
}

void Frame_7()
{
    Frame7 *frame = new Frame7("SEQUANS 4G/5G", wxDefaultPosition, wxSize(800, 600));
    frame->Show(true);
}

void Frame_8()
{
    Frame8 *frame = new Frame8("SEQUANS 4G/5G", wxDefaultPosition, wxSize(800, 600));
    frame->Show(true);
}

void Frame_9()
{
    Frame9 *frame = new Frame9("SEQUANS 4G/5G", wxDefaultPosition, wxSize(800, 600));
    frame->Show(true);
}

bool MyApp::OnInit()
{
    // Frame_1(); // OK
    // Frame_2(); // OK
    // Frame_3(); // OK
    // Frame_4(); // OK
    // Frame_5(); // OK
    // Frame_6(); // OK
    // Frame_7(); // OK
    // Frame_8(); // OK
    Frame_9();

    return true;
}
