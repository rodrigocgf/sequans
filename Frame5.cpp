#include "Frame5.h"

Frame5::Frame5(const wxString &title, const wxPoint &pos, const wxSize &size)
    : wxFrame(nullptr, wxID_ANY, title, pos, size)
{
    //Scenario1();
    Scenario2();
}

void Frame5::Scenario2()
{
    const auto margin = FromDIP(10);

    auto mainSizer = new wxBoxSizer(wxVERTICAL);
    wxPanel *panel = new wxPanel(this, wxID_ANY);
    this->SetBackgroundColour(panel->GetBackgroundColour());

    auto sizer = new wxGridBagSizer(margin, margin);

    auto nameLabel = new wxStaticText(panel, wxID_ANY, "File Name: ");
    auto kindLabel = new wxStaticText(panel, wxID_ANY, "Kind: ");
    auto sizeLabel = new wxStaticText(panel, wxID_ANY, "Size: ");
    auto dimensionsLabel = new wxStaticText(panel, wxID_ANY, "Dimensions: ");

    auto kindValue = new wxStaticText(panel, wxID_ANY, "PNG");
    auto sizeValue = new wxStaticText(panel, wxID_ANY, "1.2 MB");
    auto dimensionsValue = new wxStaticText(panel, wxID_ANY, "1024 x 768");

    auto nameText = new wxTextCtrl(panel, wxID_ANY);
    nameText->SetEditable(false);

    auto loadButton = new wxButton(panel, wxID_ANY, "Load ...");

    auto nameFormSizer = new wxBoxSizer(wxHORIZONTAL);
    nameFormSizer->Add(nameText, 1, wxEXPAND | wxRIGHT, margin);
    nameFormSizer->Add(loadButton, 0, wxALIGN_CENTER_VERTICAL);

    auto bitmap = new BufferedBitmap(panel, wxID_ANY, wxBitmap(wxSize(1, 1)), wxDefaultPosition, FromDIP(wxSize(400, 200)));
    bitmap->SetBackgroundColour(wxColour(0, 0, 0));

    auto col0 = new wxPanel(panel, wxID_ANY, wxDefaultPosition, FromDIP(wxSize(20, 20)));
    auto col1 = new wxPanel(panel, wxID_ANY, wxDefaultPosition, FromDIP(wxSize(20, 20)));
    auto col2 = new wxPanel(panel, wxID_ANY, wxDefaultPosition, FromDIP(wxSize(20, 20)));
    auto col3 = new wxPanel(panel, wxID_ANY, wxDefaultPosition, FromDIP(wxSize(20, 20)));

    col0->SetBackgroundColour(wxColour(200, 200, 0));
    col1->SetBackgroundColour(wxColour(200, 0, 200));
    col2->SetBackgroundColour(wxColour(0, 200, 200));
    col3->SetBackgroundColour(wxColour(100, 100, 200));

    sizer->Add(col0, {0, 0}, {1, 1}, wxEXPAND);
    sizer->Add(col1, {0, 1}, {1, 1}, wxEXPAND);
    sizer->Add(col2, {0, 2}, {1, 1}, wxEXPAND);
    sizer->Add(col3, {0, 3}, {1, 1}, wxEXPAND);

    sizer->Add(nameLabel, {1, 0}, {1, 1}, wxALIGN_RIGHT | wxALIGN_CENTER_VERTICAL);
    sizer->Add(kindLabel, {2, 0}, {1, 1}, wxALIGN_RIGHT );
    sizer->Add(sizeLabel, {3, 0}, {1, 1}, wxALIGN_RIGHT);
    sizer->Add(dimensionsLabel, {4, 0}, {1, 1}, wxALIGN_RIGHT);

    sizer->Add(kindValue, {2, 1}, {1, 1}, wxEXPAND);
    sizer->Add(sizeValue, {3, 1}, {1, 1}, wxEXPAND);
    sizer->Add(dimensionsValue, {4, 1}, {1, 1}, wxEXPAND);

    // sizer->Add(nameText, {1, 1}, {1, 2}, wxEXPAND);
    // sizer->Add(loadButton, {1, 3}, {1, 1}, wxEXPAND);
    sizer->Add(nameFormSizer, {1, 1}, {1, 3}, wxEXPAND);
    sizer->Add(bitmap, {2, 2}, {3, 2}, wxEXPAND);

    sizer->AddGrowableRow(3);
    sizer->AddGrowableCol(2);

    panel->SetSizer(sizer);

    mainSizer->Add(panel, 1, wxEXPAND | wxALL, margin);
    this->SetSizerAndFit(mainSizer);

    wxInitAllImageHandlers();

    loadButton->Bind(wxEVT_BUTTON, [=](wxCommandEvent &event)
                     { 
        wxFileDialog openFileDialog(this, "Open Image", "", "", "Image files(*.png;*.jpg;*.jpeg)"); 
        if ( openFileDialog.ShowModal() == wxID_CANCEL)
        {
            return;
        }

        wxImage image;
        if ( !image.LoadFile(openFileDialog.GetPath()))
        {
            wxMessageBox("Failed to load image");
            return;
        }

        // set bitmap
        bitmap->SetBitmap(wxBitmap(image));
        nameText->SetValue(openFileDialog.GetPath());

        wxFile file(openFileDialog.GetPath());

        kindValue->SetLabel(wxFileName(openFileDialog.GetPath()).GetExt().Upper());
        sizeValue->SetLabel(wxString::Format("%d KB", static_cast<int>(file.Length() / 1024)));

        dimensionsValue->SetLabel(wxString::Format("%d x %d", image.GetWidth(), image.GetHeight()));

        this->Layout();
        this->Fit(); 
    });
}

void Frame5::Scenario1()
{
    const auto margin = FromDIP(10);

    auto mainSizer = new wxBoxSizer(wxVERTICAL);
    wxPanel *panel = new wxPanel(this, wxID_ANY);
    this->SetBackgroundColour(panel->GetBackgroundColour());

    auto sizer = new wxGridBagSizer(margin, margin);

    std::vector<std::pair<wxGBPosition, wxGBSpan>> items = {
        {{0, 0}, {1, 2}},
        {{1, 0}, {1, 1}},
        {{2, 0}, {1, 1}},
        {{3, 0}, {1, 2}},
        {{4, 0}, {1, 3}},
        {{1, 1}, {1, 1}},
        {{2, 1}, {1, 1}},
        {{0, 2}, {4, 1}}};

    for (auto &item : items)
    {
        auto initialSize = sizer->GetEmptyCellSize() * 2;

        if ( item.first == wxGBPosition(1,0))
        {
            initialSize.SetWidth(FromDIP(100));
        }

        auto p = new wxPanel(panel, wxID_ANY, wxDefaultPosition, initialSize);
        p->SetBackgroundColour(wxColour(100, 100, 200));

        sizer->Add(p, item.first, item.second, wxEXPAND);
    }

    sizer->AddGrowableRow(0, 2);
    sizer->AddGrowableRow(4, 1);

    sizer->AddGrowableCol(1);
    sizer->AddGrowableCol(2);

    sizer->SetMinSize(FromDIP(wxSize(600, 400)));

    panel->SetSizer(sizer);

    mainSizer->Add(panel, 1, wxEXPAND | wxALL, margin);
    this->SetSizerAndFit(mainSizer);
}