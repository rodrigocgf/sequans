#pragma once

#include <wx/wx.h>
#include <wx/gbsizer.h>
#include <wx/panel.h>
#include <wx/sizer.h>
#include <wx/tglbtn.h>

#include "IMiddlePanel.h"
#include "UplinkFRCPanel.h"
#include "DownlinkPanel.h"

class MiddlePanel : public wxPanel, public IMiddlePanel
{
public:
    MiddlePanel(wxWindow *parent,  wxWindowID id = wxID_ANY, const wxPoint &pos = wxDefaultPosition, const wxSize &size = wxDefaultSize)
        : wxPanel(parent, id, pos, size)
    {
        Scenario1();
    }

private:
    wxBoxSizer *verticalSizer;

    UplinkFRCPanel *uplinkFRCPanel;
    DownlinkPanel *downlinkPanel;

    void Scenario1();

    void OnDownlinkClick(wxCommandEvent &event);
    void OnUplinkClick(wxCommandEvent &event);
    void OnNRTestModelsClick(wxCommandEvent &event);
    void OnDownlinkFRCClick(wxCommandEvent &event);
    void OnUplinkFRCClick(wxCommandEvent &event);
};