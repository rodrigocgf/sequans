#pragma once

#include <wx/wx.h>
#include <wx/dcclient.h>
#include <wx/sizer.h>
#include <wx/dcclient.h>
#include <iostream>
#include <type_traits>
#include <string>
#include <cmath>
#include <memory>
#include <functional>

struct Time
{
};
struct Frequency
{
};

template <typename Domain, int Width = 800, int Height = 300>
class GraphPanel : public wxPanel
{
    double v(double t, double A, double w0);

public:
    GraphPanel(wxWindow *parent, std::function<double(double)> f, wxWindowID id = wxID_ANY,
               const wxPoint &pos = wxDefaultPosition, const wxSize &size = wxDefaultSize)
        : wxPanel(parent, id, pos, wxSize(Width, Height)), m_function(f)
    {
        Bind(wxEVT_PAINT, &GraphPanel::OnPaint, this);
        SetSize(Width, Height);
        
        //Layout();
    }

    void OnPaint(wxPaintEvent &event);


private:
    std::function<double(double)> m_function;

    void DrawGraph(wxDC &dc);


    std::shared_ptr<double> timeValuesPtr;
    std::shared_ptr<double> frequencyValuesPtr;
    void CalculateFourierTransform(std::function<double(double)> timeFunction);

    double FourierTransform(int f);
};

template <typename Domain, int Width, int Height>
void GraphPanel<Domain, Width, Height>::CalculateFourierTransform(std::function<double(double)> timeFunction)
{
    double T = 1.0;
    double startTime = -2.5*T;
    double endTime = 2.5*T;
    double step = 0.2 * T;

    int numPoints = static_cast<int>((endTime - startTime) / step) + 1;

    timeValuesPtr.reset(new double[numPoints]);
    frequencyValuesPtr.reset(new double[numPoints]);

    // Calculate time and frequency domain values
    for (int i = 0; i < numPoints; ++i)
    {
        double t = startTime + i * step;
        timeValuesPtr.get()[i] = t;

        double sum = 0.0;
        // Perform numerical integration for each frequency component
        for (int n = 1; n <= 10; n++)
        { // Assuming 10 harmonics for demonstration
            sum += timeFunction(t) * std::cos(2 * M_PI * n * t) * step;
        }
        frequencyValuesPtr.get()[i] = sum / M_PI; // Normalize by 1/pi for simplicity
    }

    // Print the results
    std::cout << "Time domain:\t\tFrequency domain:" << std::endl;
    for (int i = 0; i < numPoints; ++i)
    {
        std::cout << timeValuesPtr.get()[i] << "\t\t" << frequencyValuesPtr.get()[i] << std::endl;
    }
}

template <typename Domain, int Width, int Height>
double GraphPanel<Domain, Width, Height>::FourierTransform(int f)
{
    return frequencyValuesPtr.get()[f];
}

/*
// Fourier Transform function
void FourierTransform(double (*timeFunction)(double), double startTime, double endTime, double step) {
    // Number of points in the time domain
    int numPoints = static_cast<int>((endTime - startTime) / step) + 1;

    // Arrays to store time and frequency domain values
    double* timeValues = new double[numPoints];
    double* frequencyValues = new double[numPoints];

    // Calculate time and frequency domain values
    for (int i = 0; i < numPoints; ++i) {
        double t = startTime + i * step;
        timeValues[i] = t;

        double sum = 0.0;
        // Perform numerical integration for each frequency component
        for (int n = 1; n <= 10; n++) { // Assuming 10 harmonics for demonstration
            sum += timeFunction(t) * std::sin(2 * M_PI * n * t) * step;
        }
        frequencyValues[i] = sum / M_PI; // Normalize by 1/pi for simplicity
    }

    // Print the results
    std::cout << "Time domain:\t\tFrequency domain:" << std::endl;
    for (int i = 0; i < numPoints; ++i) {
        std::cout << timeValues[i] << "\t\t" << frequencyValues[i] << std::endl;
    }

    // Clean up memory
    delete[] timeValues;
    delete[] frequencyValues;
}
*/

/*
// Define the FourierTransform function using shared pointer
std::shared_ptr<double[]> FourierTransform(double (*timeFunction)(double), double T, int numHarmonics, double duration) {
    int numSamples = static_cast<int>(duration / T);
    std::shared_ptr<double[]> result(new double[numSamples], std::default_delete<double[]>());

    for (int n = 0; n < numSamples; ++n) {
        double sum = 0.0;
        for (int k = 1; k <= numHarmonics; ++k) {
            double omega = 2.0 * M_PI * k / T;
            sum += timeFunction(n * T) * std::cos(omega * n * T);
        }
        result[n] = sum;
    }

    return result;
}
*/



template <typename Domain, int Width, int Height>
void GraphPanel<Domain, Width, Height>::OnPaint(wxPaintEvent &event)
{
    wxPaintDC dc(this);
    DrawGraph(dc);
}

template <typename Domain, int Width, int Height>
void GraphPanel<Domain, Width, Height>::DrawGraph(wxDC &dc)
{
    int width, height;
    GetSize(&width, &height);

    dc.SetBackground(wxColour(230, 230, 230));
    //dc.SetBackground(wxBrush(wxColour(255, 255, 255)));
    dc.Clear();

    // Draw x-axis
    dc.SetPen(wxPen(wxColour(0, 0, 128), 2)); // Blue color for x-axis
    dc.DrawLine(0, height / 2, width, height / 2);
    dc.DrawLine(width - 10, height / 2 - 5, width, height / 2);
    dc.DrawLine(width - 10, height / 2 + 5, width, height / 2);

    dc.DrawText("t", width - 20, height / 2 + 5); // Label for x-axis

    // Draw y-axis
    dc.SetPen(wxPen(wxColour(0, 0, 128), 2)); // Blue color for y-axis
    dc.DrawLine(width / 2, 0, width / 2, height);
    dc.DrawLine(width / 2 - 5, 10, width / 2, 0);
    dc.DrawLine(width / 2 + 5, 10, width / 2, 0);

    dc.DrawText("s(t)", width / 2 + 5, 5); // Label for y-axis

    // Scale and draw the graph
    dc.SetPen(wxPen(wxColour(0, 0, 0), 1)); // Black color for the graph

    // Draw grid
    for (int i = -width; i < width; i += 20)
    {
        dc.DrawLine(i, -height, i, height);
    }
    for (int i = -height; i < height; i += 20)
    {
        dc.DrawLine(-width, i, width, i);
    }

    dc.SetPen(wxPen(wxColour(128, 0, 0), 2)); // Red color for the graph

    double scale = height / 2.0;

    if (std::is_same<Domain, Time>::value)
    {
        for (double t = 0; t < width; t += 0.1)
        {
            double value = 0.0;
            value = m_function(static_cast<double>(t - width / 2) / 10.0);
            int y = height / 2 - static_cast<int>(value * scale / height);
            dc.DrawPoint(t, y);
        }
    }

    if (std::is_same<Domain, Frequency>::value)
    {
        CalculateFourierTransform(m_function);
        for (int f = 0; f < width; f += 1)
        {
            double value = 0.0;
            
            
            value = FourierTransform(f);
            value = 0.0;

            int y = height / 2 - static_cast<int>(value * scale / height);
            dc.DrawPoint(f, y);
        }
    }
}

