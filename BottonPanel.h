#pragma once

#include <wx/wx.h>
#include <wx/listctrl.h>
#include <wx/gbsizer.h>
#include <wx/image.h>
#include <wx/panel.h>
#include <wx/sizer.h>
#include <wx/tglbtn.h>

class BottonPanel : public wxPanel
{
public:
    BottonPanel(wxWindow *parent, wxWindowID id = wxID_ANY, const wxPoint &pos = wxDefaultPosition, const wxSize &size = wxDefaultSize)
        : wxPanel(parent, id, pos, size)
    {
        wxInitAllImageHandlers();

        Scenario1();
    }

private:
    wxBoxSizer *verticalSizer;

    void Scenario1();

    //void Scenario2();
};