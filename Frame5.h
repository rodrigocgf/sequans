#pragma once

#include <wx/wx.h>

#include <wx/gbsizer.h>
#include <vector>

#include <wx/file.h>
#include <wx/filename.h>
#include <wx/filedlg.h>
#include <wx/splitter.h>
#include <wx/textctrl.h>
#include <wx/listctrl.h>
#include <wx/treectrl.h>
#include "bufferedbitmap.h"

class Frame5 : public wxFrame
{
public:
    Frame5(const wxString &title, const wxPoint &pos, const wxSize &size);

    void OnTreeItemSelect(wxTreeEvent &event);

    void OnTreeItemSelect1(wxTreeEvent &event);

private:
    void Scenario1();
    void Scenario2();
};