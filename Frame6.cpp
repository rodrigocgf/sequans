#include "Frame6.h"

Frame6::Frame6(const wxString &title, const wxPoint &pos, const wxSize &size)
    : wxFrame(nullptr, wxID_ANY, title, pos, size)
{
    Scenario1();
}

void Frame6::CreateLabelsUplinkFRC(wxPanel *panel)
{
    frequenceRangeLabel = new wxStaticText(panel, wxID_ANY, "Frequency range: ");
    mcsLabel = new wxStaticText(panel, wxID_ANY, "MCS: ");
    fixedReferenceChannelLabel = new wxStaticText(panel, wxID_ANY, "Fixed reference channel: ");
    channelBandwidthLabel = new wxStaticText(panel, wxID_ANY, "Channel bandwidth (MHz): ");
    duplexModeLabel = new wxStaticText(panel, wxID_ANY, "Duplex mode: ");
    puschLocationLabel = new wxStaticText(panel, wxID_ANY, "PUSH location: ");
    rbOffsetLabel = new wxStaticText(panel, wxID_ANY, "RB offset: ");
    subframesLabel = new wxStaticText(panel, wxID_ANY, "Subframes: ");
    cellIdentityLabel = new wxStaticText(panel, wxID_ANY, "Cell identity: ");
    rntiLabel = new wxStaticText(panel, wxID_ANY, "RNTI: ");

    windowingSourceLabel = new wxStaticText(panel, wxID_ANY, "Windowing source: ");
    windowingLabel = new wxStaticText(panel, wxID_ANY, "Windowing (%): ");
    sampleRateSourceLabel = new wxStaticText(panel, wxID_ANY, "Sample rate source: ");
}

void Frame6::CreateComboBoxesUplinkFRC(wxPanel *panel)
{
    wxString choices[] = {
        "FR1(410 MHz - 7.125 GHz)",
        "FR2(24.25 GHz - 71 GHz)"};
    m_cbFreqRange = new wxComboBox(panel, wxID_ANY, choices[0], wxDefaultPosition,
                                   wxDefaultSize, WXSIZEOF(choices), choices, wxCB_DROPDOWN);
    m_cbFreqRange->SetSelection(0);

    wxString choicesMcs[] = {
        "QPSK, R=1/3",
        "16QAM, R=2/3",
        "QPSK, R=193/1024",
        "16QAM, R=658/1024",
        "64QAM, R=567/1024"};

    m_cbMcs = new wxComboBox(panel, wxID_ANY, choicesMcs[0], wxDefaultPosition,
                             wxDefaultSize, WXSIZEOF(choicesMcs), choicesMcs, wxCB_DROPDOWN);
    m_cbMcs->SetSelection(0);

    wxString choiceFixedRefChannel[] = {
        "G-FR1-A5-1 (15 kHz SCS, 25 RBs)",
        "G-FR1-A5-2 (15 kHz SCS, 52 RBs)",
        "G-FR1-A5-3 (15 kHz SCS, 106 RBs)",
        "G-FR1-A5-4 (30 kHz SCS, 24 RBs)",
        "G-FR1-A5-5 (30 kHz SCS, 51 RBs)",
        "G-FR1-A5-6 (30 kHz SCS, 106 RBs)",
        "G-FR1-A5-7 (30 kHz SCS, 273 RBs)",
        "G-FR1-A5-8 (15 kHz SCS, 25 RBs)",
        "G-FR1-A5-9 (15 kHz SCS, 52 RBs)",
        "G-FR1-A5-10 (15 kHz SCS, 106 RBs)",
        "G-FR1-A5-11 (30 kHz SCS, 24 RBs)",
        "G-FR1-A5-12 (30 kHz SCS, 51 RBs)",
        "G-FR1-A5-13 (30 kHz SCS, 106 RBs)",
        "G-FR1-A5-14 (30 kHz SCS, 273 RBs)"};
    m_cbFixedRefChannel = new wxComboBox(panel, wxID_ANY, choiceFixedRefChannel[0], wxDefaultPosition,
                                         wxDefaultSize, WXSIZEOF(choiceFixedRefChannel), choiceFixedRefChannel, wxCB_DROPDOWN);
    m_cbFixedRefChannel->SetSelection(0);

    wxString choicesDuplexMode[] = {
        "TDD",
        "FDD"};
    m_cbDuplexMode = new wxComboBox(panel, wxID_ANY, choicesDuplexMode[0], wxDefaultPosition,
                                    wxDefaultSize, WXSIZEOF(choicesDuplexMode), choicesDuplexMode, wxCB_DROPDOWN);
    m_cbDuplexMode->SetSelection(0);
}

void Frame6::AddElementsToSizerUplinkFRCAlt(wxGridBagSizer *sizer, const int margin)
{
    auto freqRangeLabelSizer = new wxBoxSizer(wxHORIZONTAL);
    freqRangeLabelSizer->Add(frequenceRangeLabel, 0, wxRIGHT, margin);
    freqRangeLabelSizer->Add(m_cbFreqRange, 1, wxLEFT, margin);
    // freqRangeLabelSizer->Add(frequenceRangeLabel, 1, wxEXPAND | wxRIGHT, margin);
    // freqRangeLabelSizer->Add(m_cbFreqRange, 0, wxALIGN_CENTER_VERTICAL);
    sizer->Add(freqRangeLabelSizer, {1, 0}, {1, 3}, wxEXPAND);

    auto mcsFormSizer = new wxBoxSizer(wxHORIZONTAL);
    // mcsFormSizer->Add(mcsLabel, 0, wxRIGHT, margin);
    mcsFormSizer->Add(mcsLabel, 0, wxRIGHT);
    // mcsFormSizer->Add(m_cbMcs, 1, wxRIGHT);
    mcsFormSizer->Add(m_cbMcs, 1, wxLEFT);
    // mcsFormSizer->Add(mcsLabel, 1, wxEXPAND | wxRIGHT, margin);
    // mcsFormSizer->Add(m_cbMcs, 0, wxALIGN_CENTER_VERTICAL);
    sizer->Add(mcsFormSizer, {2, 0}, {1, 3}, wxEXPAND);

    auto fixedRefChanLabSizer = new wxBoxSizer(wxHORIZONTAL);
    fixedRefChanLabSizer->Add(fixedReferenceChannelLabel, 0, wxEXPAND | wxRIGHT, margin);
    fixedRefChanLabSizer->Add(m_cbFixedRefChannel, 1, wxALIGN_CENTER_VERTICAL);
    sizer->Add(fixedRefChanLabSizer, {3, 0}, {1, 3}, wxEXPAND);

    sizer->Add(channelBandwidthLabel, {4, 0}, {1, 1}, wxALIGN_RIGHT);

    auto duplexModeLabelSizer = new wxBoxSizer(wxHORIZONTAL);
    duplexModeLabelSizer->Add(duplexModeLabel, 0, wxEXPAND | wxRIGHT, margin);
    duplexModeLabelSizer->Add(m_cbDuplexMode, 1, wxALIGN_CENTER_VERTICAL);
    sizer->Add(duplexModeLabelSizer, {5, 0}, {1, 1}, wxEXPAND);

    sizer->Add(puschLocationLabel, {6, 0}, {1, 1}, wxALIGN_RIGHT);
    sizer->Add(rbOffsetLabel, {7, 0}, {1, 1}, wxALIGN_RIGHT);
    sizer->Add(subframesLabel, {8, 0}, {1, 1}, wxALIGN_RIGHT);
    sizer->Add(cellIdentityLabel, {9, 0}, {1, 1}, wxALIGN_RIGHT);
    sizer->Add(rntiLabel, {10, 0}, {1, 1}, wxALIGN_RIGHT);

    sizer->Add(windowingSourceLabel, {12, 0}, {1, 1}, wxALIGN_RIGHT);
    sizer->Add(windowingLabel, {13, 0}, {1, 1}, wxALIGN_RIGHT);
    sizer->Add(sampleRateSourceLabel, {14, 0}, {1, 1}, wxALIGN_RIGHT);
}

void Frame6::AddElementsToSizerUplinkFRC(wxGridBagSizer *sizer, const int margin)
{

    sizer->Add(frequenceRangeLabel, {1, 0}, {1, 1}, wxALIGN_RIGHT);
    sizer->Add(m_cbFreqRange, {1, 1}, {1, 1}, wxALIGN_LEFT);

    sizer->Add(mcsLabel, {2, 0}, {1, 1}, wxALIGN_RIGHT);
    sizer->Add(m_cbMcs, {2, 1}, {1, 1}, wxALIGN_LEFT);

    sizer->Add(fixedReferenceChannelLabel, {3, 0}, {1, 1}, wxALIGN_RIGHT);
    sizer->Add(m_cbFixedRefChannel, {3, 1}, {1, 1}, wxALIGN_LEFT);

    sizer->Add(channelBandwidthLabel, {4, 0}, {1, 1}, wxALIGN_RIGHT);

    sizer->Add(duplexModeLabel, {5, 0}, {1, 1}, wxALIGN_RIGHT);
    sizer->Add(m_cbDuplexMode, {5, 1}, {1, 1}, wxALIGN_LEFT);

    sizer->Add(puschLocationLabel, {6, 0}, {1, 1}, wxALIGN_RIGHT);
    sizer->Add(rbOffsetLabel, {7, 0}, {1, 1}, wxALIGN_RIGHT);
    sizer->Add(subframesLabel, {8, 0}, {1, 1}, wxALIGN_RIGHT);
    sizer->Add(cellIdentityLabel, {9, 0}, {1, 1}, wxALIGN_RIGHT);
    sizer->Add(rntiLabel, {10, 0}, {1, 1}, wxALIGN_RIGHT);

    sizer->Add(windowingSourceLabel, {12, 0}, {1, 1}, wxALIGN_RIGHT);
    sizer->Add(windowingLabel, {13, 0}, {1, 1}, wxALIGN_RIGHT);
    sizer->Add(sampleRateSourceLabel, {14, 0}, {1, 1}, wxALIGN_RIGHT);
}

void Frame6::Scenario1()
{
    const auto margin = FromDIP(10);

    auto mainSizer = new wxBoxSizer(wxVERTICAL);
    wxPanel *panel = new wxPanel(this, wxID_ANY);
    SetBackgroundColour(panel->GetBackgroundColour());

    CreateLabelsUplinkFRC(panel);
    CreateComboBoxesUplinkFRC(panel);
   
    auto sizer = new wxGridBagSizer(margin, margin);
    AddElementsToSizerUplinkFRC(sizer, margin);

    panel->SetSizer(sizer);

    mainSizer->Add(panel, 1, wxEXPAND | wxALL, margin);
    SetSizerAndFit(mainSizer);

    SetSize(1200, 800);
}