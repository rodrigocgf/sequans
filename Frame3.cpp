#include "Frame3.h"

Frame3::Frame3(const wxString &title, const wxPoint &pos, const wxSize &size)
    : wxFrame(NULL, wxID_ANY, title, pos, size)
{

    // Create a main panel
    mainPanel = new wxPanel(this);
    mainSizer = new wxBoxSizer(wxHORIZONTAL); // Horizontal sizer

    // Create the left panel (20%)
    leftPanel = new wxPanel(mainPanel);
    leftSizer = new wxBoxSizer(wxVERTICAL);
    leftPanel->SetBackgroundColour(*wxLIGHT_GREY);

    // Create the right panel (80%)
    rightPanel = new wxPanel(mainPanel);
    rightSizer = new wxBoxSizer(wxVERTICAL);

    descriptionCtrl = new wxTextCtrl(rightPanel, wxID_ANY, "", wxDefaultPosition, wxDefaultSize, wxTE_MULTILINE | wxTE_READONLY | wxVSCROLL);
    descriptionCtrl->SetBackgroundColour(*wxLIGHT_GREY);
    rightSizer->Add(descriptionCtrl, 4, wxEXPAND);

    rightPanel->SetBackgroundColour(*wxWHITE); // Blue background
    rightPanel->SetSizer(rightSizer);

    // Add contents to left and right panels (e.g., controls, widgets)
    treeCtrl = new wxTreeCtrl(leftPanel, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTR_DEFAULT_STYLE);

    wxTreeItemId rootItem4G = treeCtrl->AddRoot("4G");
    wxTreeItemId subItem4G_1 = treeCtrl->AppendItem(rootItem4G, "1)");
    treeCtrl->AppendItem(subItem4G_1, "1.1)");
    treeCtrl->AppendItem(subItem4G_1, "1.2)");

    wxTreeItemId subItem4G_2 = treeCtrl->AppendItem(rootItem4G, "2)");
    treeCtrl->AppendItem(subItem4G_2, "2.1)");
    treeCtrl->AppendItem(subItem4G_2, "2.2)");

    treeCtrl->Expand(rootItem4G);

    treeCtrl1 = new wxTreeCtrl(leftPanel, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTR_DEFAULT_STYLE);
    wxTreeItemId rootItem5G = treeCtrl1->AddRoot("5G");
    wxTreeItemId subItem5G_1 = treeCtrl1->AppendItem(rootItem5G, "1)");
    treeCtrl1->AppendItem(subItem5G_1, "1.1)");
    treeCtrl1->AppendItem(subItem5G_1, "1.2)");

    wxTreeItemId subItem5G_2 = treeCtrl1->AppendItem(rootItem5G, "2)");
    treeCtrl1->AppendItem(subItem5G_2, "2.1)");
    treeCtrl1->AppendItem(subItem5G_2, "2.2)");

    treeCtrl1->Expand(rootItem5G);

    treeCtrl->Bind(wxEVT_TREE_SEL_CHANGED, &Frame3::OnTreeItemSelect, this);
    treeCtrl1->Bind(wxEVT_TREE_SEL_CHANGED, &Frame3::OnTreeItemSelect1, this);

    leftSizer->Add(treeCtrl, 1, wxEXPAND);
    leftSizer->Add(treeCtrl1, 1, wxEXPAND);
    leftPanel->SetSizer(leftSizer);

    // Add the left and right panels to the main sizer with proportions
    mainSizer->Add(leftPanel, 2, wxEXPAND | wxALL, 5);  // 2/10 (20%)
    mainSizer->Add(rightPanel, 8, wxEXPAND | wxALL, 5); // 8/10 (80%)

    // Set the main sizer for the main panel
    mainPanel->SetSizer(mainSizer);

    SetSize(1200, 800);
}

void Frame3::OnTreeItemSelect(wxTreeEvent &event)
{
    wxTreeItemId itemId = event.GetItem();
    if (itemId.IsOk())
    {
        wxString text = treeCtrl->GetItemText(itemId);
        descriptionCtrl->Clear();
        for (int i = 1; i <= 100; i++)
        {
            descriptionCtrl->AppendText("[" + text + "] : " + wxString::Format("%d", i) + "\n");
        }
    }
}

void Frame3::OnTreeItemSelect1(wxTreeEvent &event)
{
    wxTreeItemId itemId = event.GetItem();
    if (itemId.IsOk())
    {
        wxString text = treeCtrl1->GetItemText(itemId);
        descriptionCtrl->Clear();
        for (int i = 1; i <= 100; i++)
        {
            descriptionCtrl->AppendText("[" + text + "] : " + wxString::Format("%d", i) + "\n");
        }
    }
}