#pragma once

#include <wx/wx.h>
#include <wx/gbsizer.h>
#include <wx/panel.h>
#include <wx/sizer.h>
#include <wx/tglbtn.h>

class UplinkFRCPanel : public wxPanel
{
public:
    UplinkFRCPanel(wxWindow *parent, wxWindowID id = wxID_ANY, const wxPoint &pos = wxDefaultPosition, const wxSize &size = wxDefaultSize)
        : wxPanel(parent, id, pos, size)
    {
        Scenario1();
    }
    void ShowContentUplinkFRC(bool expanded = true);

private:
    void Scenario1();

    void OnArrowClickUplinkFRC(wxMouseEvent &event);
    void CreateArrowUplinkFRC(wxPanel *panel);
    void CreateLabelsUplinkFRC(wxPanel *panel);
    void CreateComboBoxesUplinkFRC(wxPanel *panel);
    void AddElementsToSizerUplinkFRC(const int margin);

    wxBoxSizer *mainSizer;
    bool expandedUplinkFRC;

    wxPanel *panelUplinkFRC;
    wxStaticText *arrowLabelUplinkFRC;
    wxGridBagSizer *arrowLabelUplinkFRCSizer;

    wxGridBagSizer *arrowLabelDownlinkSizer;
    wxGridBagSizer *contentSizerUplinkFRC;

    using ST_UPLINKFRC = struct {
        wxComboBox *m_cbFreqRange;
        wxComboBox *m_cbMcs;
        wxComboBox *m_cbFixedRefChannel;
        wxComboBox *m_cbDuplexMode;

        wxStaticText *frequenceRangeLabel;
        wxStaticText *mcsLabel;
        wxStaticText *fixedReferenceChannelLabel;
        wxStaticText *channelBandwidthLabel;
        wxStaticText *duplexModeLabel;
        wxStaticText *puschLocationLabel;
        wxStaticText *rbOffsetLabel;
        wxStaticText *subframesLabel;
        wxStaticText *cellIdentityLabel;
        wxStaticText *rntiLabel;
        wxStaticText *windowingSourceLabel;
        wxStaticText *windowingLabel;
        wxStaticText *sampleRateSourceLabel;
    };

    ST_UPLINKFRC m_UplinkFrc;

};