#pragma once

#include <wx/wx.h>
#include <wx/gbsizer.h>
#include <wx/image.h>
#include <wx/panel.h>
#include <wx/sizer.h>
#include <wx/tglbtn.h>

class UpperLeftPanel : public wxPanel
{
public:
    UpperLeftPanel(wxWindow *parent, wxWindowID id = wxID_ANY, const wxPoint &pos = wxDefaultPosition, const wxSize &size = wxDefaultSize)
        : wxPanel(parent, id, pos, size)
    {
        wxInitAllImageHandlers();

        Scenario1();
    }

private:
    void OnNewSession(wxCommandEvent &event);
    void OnOpenSession(wxCommandEvent &event);
    void OnSaveSession(wxCommandEvent &event);

    void Scenario1();
    
    void Scenario2();
    
};