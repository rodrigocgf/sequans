# Makefile for a simple wxWidgets program on macOS

# Define the compiler and compiler flags
CXX = g++
#CXXFLAGS = -std=c++11 `wx-config --cxxflags`
CXXFLAGS = -std=c++11 -I/Users/rodrigo/lib/wx/include/osx_cocoa-unicode-3.2 -I/Users/rodrigo/Documents/PROJETOS/C++/wxWidgets/wxWidgets-3.2.3/include -D_FILE_OFFSET_BITS=64 -DWXUSINGDLL -D__WXMAC__ -D__WXOSX__ -D__WXOSX_COCOA__ 
# Define the linker and linker flags
LD = g++
#LDFLAGS = `wx-config --libs`
LDFLAGS = -L/Users/rodrigo/lib   -framework IOKit -framework Carbon -framework Cocoa -framework QuartzCore -framework AudioToolbox -framework System -framework OpenGL -lwx_osx_cocoau_xrc-3.2 -lwx_osx_cocoau_html-3.2 -lwx_osx_cocoau_qa-3.2 -lwx_osx_cocoau_core-3.2 -lwx_baseu_xml-3.2 -lwx_baseu_net-3.2 -lwx_baseu-3.2 -lsqlite3

# Name of the output binary
TARGET = sequans

# Source file
SRC = main.cpp BottonPanel.cpp MiddlePanel.cpp WaveformPanel.cpp UpperLeftPanel.cpp DownlinkPanel.cpp UplinkFRCPanel.cpp mainFrame.cpp Frame9.cpp Frame8.cpp Frame7.cpp Frame6.cpp Frame5.cpp Frame4.cpp Frame3.cpp Frame2.cpp Frame1.cpp sqlite3.cpp

# Object files (automatically generated from source files)
OBJ = $(SRC:.cpp=.o)

# Build the executable
$(TARGET): $(OBJ)
	$(LD) -o $(TARGET) $(OBJ) $(LDFLAGS)

# Compile the source files
%.o: %.cpp
	$(CXX) -c -o $@ $< $(CXXFLAGS)

# Clean up the generated files
clean:
	rm -f $(OBJ) $(TARGET)

# Default target
all: $(TARGET)

.PHONY: all clean
