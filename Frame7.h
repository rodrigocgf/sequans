#pragma once

#include <wx/gbsizer.h>
#include <wx/wx.h>

class Frame7 : public wxFrame
{
public:
    Frame7(const wxString &title, const wxPoint &pos, const wxSize &size);
    
private:
    void Scenario1();
    void Scenario2();

    void CreateComboBoxesUplinkFRC(wxPanel *panel);
    void CreateArrowUplinkFRC(wxPanel *panel);
    void CreateLabelsUplinkFRC(wxPanel *panel);
    void CreateArrowDownlink(wxPanel *panel);
    void CreateLabelsDownlink(wxPanel *panel);
    void AddElementsToSizerUplinkFRC( const int margin);
    void AddElementsToSizerDownlink(const int margin);
    void ShowContentUplinkFRC(bool expanded);
    void ShowContentDownlink(bool expanded);
    void OnArrowClickUplinkFRC(wxMouseEvent &event);
    void OnArrowClickDownlink(wxMouseEvent &event);

    wxBoxSizer *mainSizer;

    bool expandedUplinkFRC;
    bool expandedDownlink;

    wxPanel *panelUplinkFRC;
    wxPanel *panelDownlink;
    
    wxGridBagSizer *arrowLabelUplinkFRCSizer;
    wxGridBagSizer *contentSizerUplinkFRC;

    wxGridBagSizer *arrowLabelDownlinkSizer;
    wxGridBagSizer *contentSizerDownlink;

    wxStaticText *arrowLabelDownlink;
    wxStaticText *arrowLabelUplinkFRC;

    using ST_DOWNLINK = struct {
        wxStaticText *label;
        wxStaticText *frequenceRangeLabel;
        wxStaticText *channelBandwidthLabel;
        wxStaticText *cellIdentityLabel;
        wxStaticText *subframesLabel;
        wxStaticText *initialSubframeLabel;
        wxStaticText *windowingSourceLabel;
        wxStaticText *windowingLabel;
    };

    ST_DOWNLINK m_Downlink;

    using ST_UPLINKFRC = struct {
        wxComboBox *m_cbFreqRange;
        wxComboBox *m_cbMcs;
        wxComboBox *m_cbFixedRefChannel;
        wxComboBox *m_cbDuplexMode;

        wxStaticText *frequenceRangeLabel;
        wxStaticText *mcsLabel;
        wxStaticText *fixedReferenceChannelLabel;
        wxStaticText *channelBandwidthLabel;
        wxStaticText *duplexModeLabel;
        wxStaticText *puschLocationLabel;
        wxStaticText *rbOffsetLabel;
        wxStaticText *subframesLabel;
        wxStaticText *cellIdentityLabel;
        wxStaticText *rntiLabel;
        wxStaticText *windowingSourceLabel;
        wxStaticText *windowingLabel;
        wxStaticText *sampleRateSourceLabel;
    };

    ST_UPLINKFRC m_UplinkFrc;

    using ST_NRTESTMODEL = struct {
        wxStaticText *frequenceRangeLabel;
        wxStaticText *testModelLabel;
        wxStaticText *subcarrierSpacingLabel;
        wxStaticText *channelBandWidthLabel;
        wxStaticText *duplexModeLabel;
        wxStaticText *subframesLabel;
        wxStaticText *cellIdentityLabel;
        wxStaticText *windowingSourceLabel;
        wxStaticText *windowingLabel;
        wxStaticText *sampleRateSourceLabel;
    };

    ST_NRTESTMODEL m_NRTestModel;
};